// SETUP DI BASE DEL SERVER

/*--------------*/
/* DEPENDENCIES */
/*--------------*/
// Richiesta modulo "http" dalla libreria base di node.js
var http = require("http");
/*var https = require('https'),     
	fs = require("fs");
var options = {
		key: fs.readFileSync("ssl/PrivateKey.key"),
		cert: fs.readFileSync("ssl/WebserverCertificate.crt"),
		ca: fs.readFileSync("ssl/CertificateSigningRequest.csr"),
	};*/
// Richiesta modulo "express" dalla libreria base di node.js
var express = require("express");
// Richiesta modulo "socket.io" dalla libreria base di node.js
//var io = require("socket.io");
var io = require("socket.io", {transport:["websocket"]})(http);

/*----------------*/
/* INITIALIZATION */
/*----------------*/
// Express inizializza app per essere un gestore di funzioni che è possibile fornire a un server HTTP
var app = express();

// Crea il server HTTP
var server = http.createServer(app);
//var server = https.createServer(options, app);

//  Inizializzazione di una nuova istanza di socket.io passando l'oggetto http (il server HTTP)
io = io.listen(server);
//io = io(server);

var ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;

// Imposta il server HTTP in ascolto.
if (ip == "127.0.0.1") {
	server.listen(port, function () {
		console.log("Server listening at port:" + port)
	});
}
else {
	server.listen(port, ip, function () {
		console.log("Server listening at " + ip + ":" + port);
	});

	app.enable('trust proxy');
}

// Imposta la cartella pubblica delle risorse statiche
app.use(express.static(__dirname + "/public"));

// Array per memorizzare i dati delle singole stanze
var rooms = [{
	roomName: "lobby",// nome della stanza
	roomPassword: "",// password della stanza
	users: [],// Array per memorizzare gli utenti presenti nella stanza
	sketches: [],// Array per memorizzare i tratti
	undo: [],// Array per memorizzare i tratti annullati
	messages: [],
}];

/*-----------------*/
/* PRIVATE METHODS */
/*-----------------*/
// Metodo che restitusce l'indice della stanza dato il nome
function getRoomByName(roomName){
	for (var i = rooms.length - 1; i >= 0; i--) {
		if (rooms[i].roomName == roomName){
			return i;
		}
	}
	return -1;
}
// Metodo che restitusce l'indice dell'ultimo tratto fatto da un certo autore
function getLastSketch(roomIndex, author){
	for (var i = rooms[roomIndex].sketches.length - 1; i >= 0; i--) {
		if (rooms[roomIndex].sketches[i].author == author){
			return i;
		}
	}
	return -1;
}
// Metodo che restitusce l'indice dell'ultimo tratto annullato da un certo autore
function getLastUndo(roomIndex, author){
	for (var i = rooms[roomIndex].undo.length - 1; i >= 0; i--) {
		if (rooms[roomIndex].undo[i].author == author){
			return i;
		}
	}
	return -1;
}
// Metodo privato che codifica gli strokes
function encode(roomIndex, lastSketch, x, y){
	var delta = [];
	delta[0] = rooms[roomIndex].sketches[lastSketch].stroke[rooms[roomIndex].sketches[lastSketch].stroke.length - 2].x - x;
	delta[1] = rooms[roomIndex].sketches[lastSketch].stroke[rooms[roomIndex].sketches[lastSketch].stroke.length - 2].y - y;
	var zigzag = [];
	zigzag[0] = (delta[0] << 1) ^ (delta[0] >> 31);
	zigzag[1] = (delta[1] << 1) ^ (delta[1] >> 31);
	return zigzag;
}
// Metodo che decodifica gli strokes
function decode(roomIndex, lastSketch, x, y){
	var zigzag = [];
	zigzag[0] = (x >> 1) ^ (-(x & 1));
	zigzag[1] = (y >> 1) ^ (-(y & 1));
	var delta = [];
	delta[0] = rooms[roomIndex].sketches[lastSketch].stroke[rooms[roomIndex].sketches[lastSketch].stroke.length - 1].x - zigzag[0];
	delta[1] = rooms[roomIndex].sketches[lastSketch].stroke[rooms[roomIndex].sketches[lastSketch].stroke.length - 1].y - zigzag[1];
	return delta;
}
// Metodo privato che controlla la correttezza della struttura dati di un singolo sketch
function isSketchDataValid(sketch){

	function isStrokeDataValid(stroke){
		if (stroke.length > 1) {
			for (var i = 1; i < stroke.length; i++) {
				if (
					Object.keys(stroke[i]).length === 2
					&&
					stroke[i].hasOwnProperty("x")
					&&
					stroke[i].hasOwnProperty("y")
					&&
					typeof stroke[i].x === "number"
					&&
					typeof stroke[i].y === "number"
					)
				{
					return true;
				}
			}
			return false;
		}
		else{
			return true;
		}
	}
	return (
		typeof sketch === "object"
		&&
		Object.keys(sketch).length === 8
		&&
		sketch.hasOwnProperty("canvasSize")
		&&
		sketch.hasOwnProperty("author")
		&&
		sketch.hasOwnProperty("dot")
		&&
		sketch.hasOwnProperty("stroke")
		&&
		sketch.hasOwnProperty("globalCompositeOperation")
		&&
		sketch.hasOwnProperty("lineWidth")
		&&
		sketch.hasOwnProperty("strokeStyle")
		&&
		sketch.hasOwnProperty("image")
		&&
		typeof sketch.canvasSize === "object"
		&&
		typeof sketch.author === "string"
		&&
		typeof sketch.dot === "object"
		&&
		typeof sketch.stroke === "object"
		&&
		typeof sketch.globalCompositeOperation === "string"
		&&
		typeof sketch.lineWidth === "number"
		&&
		typeof sketch.strokeStyle === "string"
		&&
		(typeof sketch.image === "string" || sketch.image === null)
		&&
		(sketch.globalCompositeOperation === "source-over"
			||
			sketch.globalCompositeOperation === "source-in"
			||
			sketch.globalCompositeOperation === "source-out"
			||
			sketch.globalCompositeOperation === "source-atop"
			||
			sketch.globalCompositeOperation === "destination-over"
			||
			sketch.globalCompositeOperation === "destination-in"
			||
			sketch.globalCompositeOperation === "destination-out"
			||
			sketch.globalCompositeOperation === "destination-atop"
			||
			sketch.globalCompositeOperation === "lighter"
			||
			sketch.globalCompositeOperation === "copy"
			||
			sketch.globalCompositeOperation === "xor"
			||
			sketch.globalCompositeOperation === "multiply"
			||
			sketch.globalCompositeOperation === "screen"
			||
			sketch.globalCompositeOperation === "overlay"
			||
			sketch.globalCompositeOperation === "darken"
			||
			sketch.globalCompositeOperation === "lighten"
			||
			sketch.globalCompositeOperation === "color-dodge"
			||
			sketch.globalCompositeOperation === "color-burn"
			||
			sketch.globalCompositeOperation === "hard-light"
			||
			sketch.globalCompositeOperation === "soft-light"
			||
			sketch.globalCompositeOperation === "difference"
			||
			sketch.globalCompositeOperation === "exclusion"
			||
			sketch.globalCompositeOperation === "hue"
			||
			sketch.globalCompositeOperation === "saturation"
			||
			sketch.globalCompositeOperation === "color"
			||
			sketch.globalCompositeOperation === "luminosity")
		&&
		(sketch.strokeStyle).match(/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i)
		&&
		Object.keys(sketch.canvasSize).length === 2
		&&
		sketch.canvasSize.hasOwnProperty("x")
		&&
		sketch.canvasSize.hasOwnProperty("y")
		&&
		(typeof sketch.canvasSize.x === "number" || (typeof sketch.canvasSize.x === "object" && sketch.canvasSize.x === null))
		&&
		(typeof sketch.canvasSize.y === "number" || (typeof sketch.canvasSize.y === "object" && sketch.canvasSize.y === null))
		&&
		sketch.dot.length === 1
		&&
		typeof sketch.dot[0] === "object"
		&&
		sketch.stroke.length >= 1
		&&
		typeof sketch.stroke[0] === "object"
		&&
		Object.keys(sketch.dot[0]).length === 2
		&&
		Object.keys(sketch.stroke[0]).length === 2
		&&
		sketch.dot[0].hasOwnProperty("x")
		&&
		sketch.dot[0].hasOwnProperty("y")
		&&
		sketch.stroke[0].hasOwnProperty("x")
		&&
		sketch.stroke[0].hasOwnProperty("y")
		&&
		(typeof sketch.dot[0].x === "number" || (typeof sketch.dot[0].x === "object" && sketch.dot[0].x === null))
		&&
		(typeof sketch.dot[0].y === "number" || (typeof sketch.dot[0].y === "object" && sketch.dot[0].y === null))
		&&
		(typeof sketch.stroke[0].x === "number" || (typeof sketch.stroke[0].x === "object" && sketch.stroke[0].x === null))
		&&
		(typeof sketch.stroke[0].y === "number" || (typeof sketch.stroke[0].y === "object" && sketch.stroke[0].y === null))
		&&
		isStrokeDataValid(sketch.stroke) === true
	);
}
// Metodo privato che ripulisce un singolo sketch dai dati ridondanti
function purgeSketch(sketch) {
	
	var purgedSketch = ({
		canvasSize: sketch.canvasSize,
		author: sketch.author,
		dot: [{
			x: sketch.dot[0].x,
			y: sketch.dot[0].y
		}],
		stroke: [{
			x: sketch.stroke[0].x,
			y: sketch.stroke[0].y
		}],
		globalCompositeOperation : sketch.globalCompositeOperation,
		lineWidth : sketch.lineWidth,
		strokeStyle : sketch.strokeStyle,
		image: sketch.image,
	});
	
	for (var i = 0; i < sketch.stroke.length; i++) {
		if (i == 0) {
			purgedSketch.stroke.push({
				x: sketch.stroke[i].x,
				y: sketch.stroke[i].y
			});
		}
		else if (!(sketch.stroke[i].x == sketch.stroke[i - 1].x && sketch.stroke[i].y == sketch.stroke[i - 1].y)) {
			purgedSketch.stroke.push({
				x: sketch.stroke[i].x,
				y: sketch.stroke[i].y
			});
		}
	}
	
	return purgedSketch;
}

/*-----------------------------*/
/* CLIENT LISTENERS & HANDLERS */
/*-----------------------------*/
// Quando un client si connette
io.on("connection", function(socket){

	// Cattura l'ip del client
	socket.userIp = socket.request.headers['x-forwarded-for'];
	if (ip == "127.0.0.1") {
		socket.userIp = (socket.request.connection.remoteAddress).split(":")[3];
	}
	else {
		socket.userIp = socket.request.headers['x-forwarded-for'];
	}
	console.log("client "+socket.userIp+" connected");
	socket.room = null;
	
	// Comunica al client che la connessione è avvenuta
	socket.emit("ServerEventConnected");

	// Quando un client richide di loggarsi con userName e password
	socket.on("ClientEventLogRequest", function(data){

		function isLogRequestDataValid(data){
			return (
				typeof data === "object"
				&&
				Object.keys(data).length === 2
				&&
				data.hasOwnProperty("userName")
				&&
				data.hasOwnProperty("userPassword")
				&&
				typeof data.userName === "string"
				&&
				typeof data.userPassword === "string"
			);
		}
		function isUserNameTaken(userName){
			for (var i = 0; i < rooms.length; i++) {
				for (var j = 0; j < rooms[i].users.length; j++) {
					if (userName === rooms[i].users[j]) {
						return true;
					}
				}
			}
			return false;
		}
		function isUserNameValid(userName){
			return (userName != null && (userName).match(/^[a-zA-Z0-9_-]{3,15}$/));
		}
		function isAlreadyLogged(){
			return (socket.hasOwnProperty("userName"));
		}

		if (isAlreadyLogged() == true || isLogRequestDataValid(data) == false || isUserNameTaken(data.userName) == true || isUserNameValid(data.userName) == false) {
			socket.emit("ServerEventLogResponse", "rejected");
		}
		else{
			socket.emit("ServerEventLogResponse", "accepted");
			
			// Memorizza l'userName del client
			socket.userName = data.userName;
			// Memorizza la password del client
			socket.userPassword = data.userPassword;

			// Sposta il client nella lobby
			socket.room = "lobby";
			socket.join(socket.room);

			// Aggiunge l'userName del client dalla struttura dati della stanza lobby
			rooms[getRoomByName(socket.room)].users.push(socket.userName);
			
			console.log("user " + socket.userName + "(" + socket.userIp + ") joined the lobby");

			// Comunica al client il suo userName
			socket.emit("ServerEventInLobby", {
				userName: socket.userName,
			});
		}
	});
	
	// Quando un client richiede di creare una stanza
	socket.on("ClientEventCreateRoomRequest", function(data){

		function isAlreadyInRoom(){
			return (socket.hasOwnProperty("room") && socket.room != null && socket.room != "lobby");
		}
		function isCreateRoomRequestDataValid(data){
			return (
				typeof data === "object"
				&&
				Object.keys(data).length === 2
				&&
				data.hasOwnProperty("roomName")
				&&
				data.hasOwnProperty("roomPassword")
				&&
				typeof data.roomName === "string"
				&&
				typeof data.roomPassword === "string"
			);
		}
		function isRoomNameTaken(roomName){
			return (getRoomByName(roomName) >= 0);
		}
		function isRoomNameValid(roomName){
			return (roomName != null && (roomName).match(/^[a-zA-Z0-9_-]{3,15}$/));
		}
		if (isAlreadyInRoom() == true || isCreateRoomRequestDataValid(data) == false || socket.room == null || isRoomNameTaken(data.roomName) == true || isRoomNameValid(data.roomName) == false) {
			socket.emit("ServerEventCreateRoomResponse", "rejected");
		}
		else{
			socket.emit("ServerEventCreateRoomResponse", "accepted");
			
			// Rimuove l'userName del client dalla struttura dati della stanza precedente
			rooms[getRoomByName(socket.room)].users.splice(rooms[getRoomByName(socket.room)].users.indexOf(socket.userName),1);

			// Crea la struttura dati della stanza corrente
			rooms.push({
				roomName: data.roomName,
				roomPassword: data.roomPassword,
				users: [socket.userName],
				sketches: [],
				undo: [],
				messages: [],
			});

			// Sposta il client nella stanza corrente
			socket.leave(socket.room);
			socket.room = data.roomName;
			socket.join(socket.room);

			console.log("user " + socket.userName + "(" + socket.userIp + ") joined room " + socket.room);
			
			// Comunica al client il suo userName e la struttura dati della stanza corrente
			socket.emit("ServerEventInRoom", {
				userName: socket.userName,
				roomName: socket.room,
				users: rooms[getRoomByName(socket.room)].users,
				sketches: rooms[getRoomByName(socket.room)].sketches,
				undo: [],
				messages: rooms[getRoomByName(socket.room)].messages,
			});
		}
	});

	// Quando un client richiede di unirsi a una stanza
	socket.on("ClientEventJoinRoomRequest", function(data){
		
		function isAlreadyInRoom(){
			return (socket.hasOwnProperty("room") && socket.room != null && socket.room != "lobby");
		}
		function isJoinRoomRequestDataValid(data){
			return (
				typeof data === "object"
				&&
				Object.keys(data).length === 2
				&&
				data.hasOwnProperty("roomName")
				&&
				data.hasOwnProperty("roomPassword")
				&&
				typeof data.roomName === "string"
				&&
				typeof data.roomPassword === "string"
			);
		}
		function roomNameExist(roomName){
			return (getRoomByName(roomName) >= 0);
		}
		if (isAlreadyInRoom() == true || isJoinRoomRequestDataValid(data) == false || socket.room == null || roomNameExist(data.roomName) == false || rooms[getRoomByName(data.roomName)].roomPassword != data.roomPassword) {
			socket.emit("ServerEventJoinRoomResponse", "rejected");
		}
		else{
			socket.emit("ServerEventJoinRoomResponse", "accepted");
			
			// Rimuove l'userName del client dalla struttura dati della stanza precedente
			rooms[getRoomByName(socket.room)].users.splice(rooms[getRoomByName(socket.room)].users.indexOf(socket.userName),1);

			// Sposta il client nella stanza corrente
			socket.leave(socket.room);
			socket.room = data.roomName;
			socket.join(socket.room);

			// Aggiunge l'userName del client dalla struttura dati della stanza corrente
			rooms[getRoomByName(socket.room)].users.push(socket.userName);

			console.log("user " + socket.userName + "(" + socket.userIp + ") joined room " + socket.room);

			// Comunica al client il suo userName e la struttura dati
			socket.emit("ServerEventInRoom", {
				userName: socket.userName,
				roomName: socket.room,
				users: rooms[getRoomByName(socket.room)].users,
				sketches: rooms[getRoomByName(socket.room)].sketches,
				undo: [],
				messages: rooms[getRoomByName(socket.room)].messages,
			});
			// Comunica a tutti gli altri l'evento "ServerEventUserJoined"
			if (socket.room != null && socket.room != "lobby") {
				socket.broadcast.to(socket.room).emit("ServerEventUserJoined", {
					userName: socket.userName,
				});
			}
		}
	});

	// Quando un client richide di uscire
	socket.on("ClientEventLogOut", function(){
		if (socket.room != null) {
			socket.leave(socket.room);
			
			if (getRoomByName(socket.room) >= 0) {
				// Rimuove l'userName del client dalla struttura dati della stanza
				rooms[getRoomByName(socket.room)].users.splice(rooms[getRoomByName(socket.room)].users.indexOf(socket.userName),1);
			}
			
			// Comunica a tutti gli altri l'evento "ServerEventUserleft"
			socket.broadcast.to(socket.room).emit("ServerEventUserleft", {
				userName: socket.userName,
			});

			//Se non ci sono più utenti connessi, aspetta 5 secondi poi cancella la struttura dati della stanza
			if (socket.room != "lobby" && getRoomByName(socket.room) >= 0 && rooms[getRoomByName(socket.room)].users.length == 0){
				rooms.splice(getRoomByName(socket.room), 1);
			}
			else if (socket.room != "lobby" && getRoomByName(socket.room) >= 0 && rooms[getRoomByName(socket.room)].users.length == 0){
				setTimeout( function() {
					if (socket.room != "lobby" && getRoomByName(socket.room) >= 0 && rooms[getRoomByName(socket.room)].users.length == 0){
						rooms.splice(getRoomByName(socket.room), 1);
					}
				}, 5000);
			}

			socket.room = null;

			console.log("user " + socket.userName + "(" + socket.userIp + ") logged out");
		}
	});
	
	// Ascolta questi eventi solo se il client è entrato in una stanza, lobby esclusa
		// Quando un client emette l'evento
		socket.on("ClientEventCursorMove", function(data){

			function isCursorMoveDataValid(data){
				return (
					typeof data === "object"
					&&
					Object.keys(data).length === 2
					&&
					data.hasOwnProperty("canvasSize")
					&&
					data.hasOwnProperty("cursor")
					&&
					typeof data.canvasSize === "object"
					&&
					typeof data.cursor === "object"
					&&
					Object.keys(data.canvasSize).length === 2
					&&
					Object.keys(data.cursor).length === 2
					&&
					data.canvasSize.hasOwnProperty("x")
					&&
					data.canvasSize.hasOwnProperty("y")
					&&
					data.cursor.hasOwnProperty("x")
					&&
					data.cursor.hasOwnProperty("y")
					&&
					typeof data.canvasSize.x === "number"
					&&
					typeof data.canvasSize.y === "number"
					&&
					typeof data.cursor.x === "number"
					&&
					typeof data.cursor.y === "number"
				);
			}
			if (isCursorMoveDataValid(data) == true && socket.room != null && socket.room != "lobby") {
				// Comunica a tutti gli altri l'evento
				socket.broadcast.to(socket.room).emit("ServerEventCursorMove", {
					userName: socket.userName,
					content: data,
				});
			}
		});

		// Quando un client emette l'evento
		socket.on("ClientEventMouseDown", function(data){
			if (isSketchDataValid(data) == true && socket.room != null && socket.room != "lobby") {

				// Ottiene l'author dal socket.userName
				data.author = socket.userName;

				// Comunica a tutti gli altri l'evento
				socket.broadcast.to(socket.room).emit("ServerEventMouseDown", {
					userName: socket.userName,
					content: data,
				});

				var roomIndex = getRoomByName(socket.room);
				rooms[roomIndex].sketches.push(data);
			}
		});

		// Quando un client emette l'evento
		socket.on("ClientEventMouseMoveStroke", function(data){

			function isMouseMoveStrokeDataValid(data){
				return (
					typeof data === "object"
					&&
					data.length === 2
					&&
					typeof data[0] === "number"
					&&
					typeof data[0] === "number"
				);
			}
			if (isMouseMoveStrokeDataValid(data) == true && socket.room != null && socket.room != "lobby") {

				var roomIndex = getRoomByName(socket.room);
				var lastSketch = getLastSketch(roomIndex, socket.userName);
				
				var decoded = decode(roomIndex, lastSketch, data[0], data[1]);

				// se il punto rilevato è ridondante non lo processa
				if (!(rooms[roomIndex].sketches[lastSketch].stroke[rooms[roomIndex].sketches[lastSketch].stroke.length - 1].x == decoded[0].x &&
					rooms[roomIndex].sketches[lastSketch].stroke[rooms[roomIndex].sketches[lastSketch].stroke.length - 1].y == decoded[1].y)) {

					// Comunica a tutti gli altri l'evento
					socket.broadcast.to(socket.room).emit("ServerEventMouseMoveStroke", {
						userName: socket.userName,
						content: data,
					});					

					// Aggiunge la coordianta in coda all'array del tratto
					rooms[roomIndex].sketches[lastSketch].stroke.push({
						x: decoded[0],
						y: decoded[1],
					});
				}
				// Comunica a tutti gli altri l'evento
				/*socket.broadcast.to(socket.room).emit("ServerEventMouseMove", {
					userName: socket.userName,
					content: data,
				});

				var roomIndex = getRoomByName(socket.room);
				var lastSketch = getLastSketch(roomIndex, socket.userName);
				
				var decoded = decode(roomIndex, lastSketch, data[0], data[1]);
				rooms[roomIndex].sketches[lastSketch].stroke.push({
					x: decoded[0],
					y: decoded[1]
				});*/
			}
		});

		// Quando un client emette l'evento
		socket.on("ClientEventMouseMoveLine", function(data){

			function isMouseMoveLineDataValid(data){
				return (
					typeof data === "object"
					&&
					Object.keys(data).length === 2
					&&
					data.hasOwnProperty("x")
					&&
					data.hasOwnProperty("y")
					&&
					typeof data.x === "number"
					&&
					typeof data.y === "number"
				);
			}
			if (isMouseMoveLineDataValid(data) == true && socket.room != null && socket.room != "lobby") {
				
				socket.broadcast.to(socket.room).emit("ServerEventMouseMoveLine", {
					userName: socket.userName,
					content: data,
				});

				var roomIndex = getRoomByName(socket.room);
				var lastSketch = getLastSketch(roomIndex, socket.userName);
				
				rooms[roomIndex].sketches[lastSketch].stroke[1] = data;
			}
		});

		// Quando un client emette l'evento
		socket.on("ClientEventMouseMoveRect", function(data){

			function isMouseMoveRectDataValid(data){
				return (
					typeof data === "object"
					&&
					Object.keys(data).length === 2
					&&
					data.hasOwnProperty("x")
					&&
					data.hasOwnProperty("y")
					&&
					typeof data.x === "number"
					&&
					typeof data.y === "number"
				);
			}
			if (isMouseMoveRectDataValid(data) == true && socket.room != null && socket.room != "lobby") {
				
				socket.broadcast.to(socket.room).emit("ServerEventMouseMoveRect", {
					userName: socket.userName,
					content: data,
				});

				var roomIndex = getRoomByName(socket.room);
				var lastSketch = getLastSketch(roomIndex, socket.userName);

				rooms[roomIndex].sketches[lastSketch].stroke[1] = {
					x: data.x,
					y: rooms[roomIndex].sketches[lastSketch].stroke[0].y
				};

				rooms[roomIndex].sketches[lastSketch].stroke[2] = {
					x: data.x,
					y: data.y
				};

				rooms[roomIndex].sketches[lastSketch].stroke[3] = {
					x: rooms[roomIndex].sketches[lastSketch].stroke[0].x,
					y: data.y
				};

				rooms[roomIndex].sketches[lastSketch].stroke[4] = {
					x: rooms[roomIndex].sketches[lastSketch].stroke[0].x,
					y: rooms[roomIndex].sketches[lastSketch].stroke[0].y
				};
			}
		});

		// Quando un client emette l'evento
		socket.on("ClientEventMouseMoveEllipse", function(data){

			function isMouseMoveEllipseDataValid(data){
				return (
					typeof data === "object"
					&&
					Object.keys(data).length === 2
					&&
					data.hasOwnProperty("mouseDownCursor")
					&&
					data.hasOwnProperty("cursor")
					&&
					typeof data.mouseDownCursor === "object"
					&&
					typeof data.cursor === "object"
					&&
					Object.keys(data.mouseDownCursor).length === 2
					&&
					Object.keys(data.cursor).length === 2
					&&
					data.mouseDownCursor.hasOwnProperty("x")
					&&
					data.mouseDownCursor.hasOwnProperty("y")
					&&
					data.cursor.hasOwnProperty("x")
					&&
					data.cursor.hasOwnProperty("y")
					&&
					typeof data.mouseDownCursor.x === "number"
					&&
					typeof data.mouseDownCursor.y === "number"
					&&
					typeof data.cursor.x === "number"
					&&
					typeof data.cursor.y === "number"
				);
			}
			if (isMouseMoveEllipseDataValid(data) == true && socket.room != null && socket.room != "lobby") {
				
				socket.broadcast.to(socket.room).emit("ServerEventMouseMoveEllipse", {
					userName: socket.userName,
					content: data,
				});

				var roomIndex = getRoomByName(socket.room);
				var lastSketch = getLastSketch(roomIndex, socket.userName);

				rooms[roomIndex].sketches[lastSketch].dot[0].x = null;
				rooms[roomIndex].sketches[lastSketch].dot[0].y = null;

				var sides = 60;

				var x1 = data.mouseDownCursor.x;
				var y1 = data.mouseDownCursor.y;

				var cx = (x1 + data.cursor.x)/2;
				var cy = (y1 + data.cursor.y)/2;

				var a = (Math.abs(x1 - data.cursor.x))/2;
				var b = (Math.abs(y1 - data.cursor.y))/2;		

				for (var i = 0; i < sides; i++) {
					theta = (i/sides)*2*Math.PI;
					rooms[roomIndex].sketches[lastSketch].stroke[i] = ({
						x: Math.floor((cx + a*Math.cos(theta))*100)/100,
						y: Math.floor((cy + b*Math.sin(theta))*100)/100
					});
				}
				rooms[roomIndex].sketches[lastSketch].stroke[sides] = ({
					x: Math.floor((cx + a*Math.cos(0))*100)/100,
					y: Math.floor((cy + b*Math.sin(0))*100)/100
				});
			}
		});

		// Quando un client emette l'evento
		socket.on("ClientEventNew", function(){
			if (socket.room != null && socket.room != "lobby") {
				// Comunica a tutti gli altri l'evento
				socket.broadcast.to(socket.room).emit("ServerEventNew", {
					userName: socket.userName,
				});
				
				var roomIndex = getRoomByName(socket.room);
				rooms[roomIndex].sketches = [];
			}
		});

		// Quando un client emette l'evento
		socket.on("ClientEventOpen", function(data){
			if (isSketchDataValid(data) == true && socket.room != null && socket.room != "lobby") {

				// Ottiene l'author dal socket.userName
				data.author = socket.userName;

				// Ripulisce dai dati ridondanti
				data = purgeSketch(data);

				// Comunica a tutti gli altri l'evento
				socket.broadcast.to(socket.room).emit("ServerEventOpen", {
					userName: socket.userName,
					content: data,
				});
				
				var roomIndex = getRoomByName(socket.room);
				rooms[roomIndex].sketches.push(data);
			}
		});

		// Quando un client emette l'evento
		socket.on("ClientEventUndo", function(data){
			if (socket.room != null && socket.room != "lobby") {
				// Comunica a tutti gli altri l'evento
				socket.broadcast.to(socket.room).emit("ServerEventUndo", {
					userName: socket.userName,
				});
				
				var roomIndex = getRoomByName(socket.room);
				var lastSketch = getLastSketch(roomIndex, socket.userName);

				if (lastSketch >= 0) {
					rooms[roomIndex].undo.push(rooms[roomIndex].sketches[lastSketch]);
					rooms[roomIndex].sketches.splice(lastSketch,1);
				}
			}
		});

		// Quando un client emette l'evento
		socket.on("ClientEventRedo", function(data){
			if (isSketchDataValid(data) == true && socket.room != null && socket.room != "lobby") {

				// Ottiene l'author dal socket.userName
				data.author = socket.userName;

				// Ripulisce dai dati ridondanti
				data = purgeSketch(data);

				// Comunica a tutti gli altri l'evento
				socket.broadcast.to(socket.room).emit("ServerEventRedo", {
					userName: socket.userName,
					content: data,
				});
				
				var roomIndex = getRoomByName(socket.room);
				var lastUndo = getLastUndo(roomIndex, socket.userName);

				if (lastUndo >= 0) {
					rooms[roomIndex].sketches.push(data);
				}
			}
		});

		// Quando un client emette l'evento "ClientEventNewMessage"
		socket.on("ClientEventNewMessage", function(data){
			
			function isNewMessageDataValid(data){
				return (typeof data === "string");
			}
			if (isNewMessageDataValid(data) == true && socket.room != null && socket.room != "lobby") {
				// Comunica a tutti quanti l'evento "ServerEventNewMessage"
				io.to(socket.room).emit("ServerEventNewMessage", {
					userName: socket.userName,
					message: data,
				});
				
				var roomIndex = getRoomByName(socket.room);
				
				rooms[roomIndex].messages.push({
					author: socket.userName,
					message: data,
				});
			}
		});

	// Quando un client emette l'evento "disconnect"
  	socket.on("disconnect", function () {

		if (getRoomByName(socket.room) >= 0) {
			// Rimuove l'userName del client dalla struttura dati della stanza
			rooms[getRoomByName(socket.room)].users.splice(rooms[getRoomByName(socket.room)].users.indexOf(socket.userName),1);
			
			console.log("user " + socket.userName + "(" + socket.userIp + ") disconnected");
		}
		else{
			console.log("client "+socket.userIp+" disconnected");
		}

		// Comunica a tutti gli altri l'evento "ServerEventUserleft"
		socket.broadcast.to(socket.room).emit("ServerEventUserleft", {
			userName: socket.userName,
		});

		//Se non ci sono più utenti connessi, aspetta 5 secondi poi cancella la struttura dati della stanza
		if (socket.room != "lobby" && getRoomByName(socket.room) >= 0 && rooms[getRoomByName(socket.room)].users.length == 0){
			setTimeout( function() {
				if (socket.room != "lobby" && getRoomByName(socket.room) >= 0 && rooms[getRoomByName(socket.room)].users.length == 0){
					rooms.splice(getRoomByName(socket.room), 1);
				}
			}, 5000);
		}
	});
});