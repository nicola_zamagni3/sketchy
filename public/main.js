document.addEventListener("DOMContentLoaded", function(){

	require([
		"./models/AuthenticationModel.js",
		"./controllers/AuthenticationController.js",
		"./models/StatusModel.js",
		"./models/WhiteBoardModel.js",
		"./controllers/WhiteBoardController.js",
		"./controllers/MenusController.js",
		"./controllers/CommandsController.js",
		"./models/PointerModel.js",
		"./controllers/PointerController.js",
		"./models/UsersListModel.js",
		"./models/ChatModel.js",
		"./controllers/ChatController.js",
		"./controllers/DialogController.js",
	]);

	function require(urls) {
		var loadedScript = 0;
		urls.forEach(function(src) {
			var script = document.createElement('script');
			script.src = src;
			script.async = false;

			script.onload = function() {
				loadedScript++;
				if (loadedScript == urls.length){
					init();
				}
			};

			document.body.appendChild(script);
		});
	}

	function init() {

		/*-------------------*/
		/* PRIVATE VARIABLES */
		/*-------------------*/
		// Variabile socket.io
		if ((location.hostname).match(/[a-z]/i)) {
			var socket = io("ws://" + location.hostname);
		}
		else{
			var socket = io();
		}

		// Instanzia le classi per l'autenticazione
		var authenticationParameters = {
				btnLogIn: "btnLogIn",
				btnCreateRoom: "btnCreateRoom",
				btnJoinRoom: "btnJoinRoom",
				btnLogOut: "btnLogOut",
			};
		var authenticationModel = new AuthenticationModel(socket, authenticationParameters);
		var authenticationController = new AuthenticationController(authenticationModel, authenticationParameters);

		// Istanzia la classe per lo status
		var statusModel = new StatusModel(socket, "status");


		// Instanzia le classi per la lavagna
		var whiteBoardModel = new WhiteBoardModel(socket, "canvas");
		
		// Instanzia la classe per i comandi
		var commandsParameters = {
				whiteBoardModel: whiteBoardModel,
				btnNew: "btnNew",
				btnOpen: "btnOpen",
				btnSave: "btnSave",
				btnExport: "btnExport",
				btnUndo: "btnUndo",
				btnRedo: "btnRedo",
				btnPencil: "btnPencil",
				btnEraser: "btnEraser",
				btnMarker: "btnMarker",
				btnLine: "btnLine",
				btnRect: "btnRect",
				btnEllipse: "btnEllipse",
				btnWidth1: "btnWidth1",
				btnWidth2: "btnWidth2",
				btnWidth3: "btnWidth3",
				btnWidth4: "btnWidth4",
				btnWidth5: "btnWidth5",
				btnColor: "btnColor",
				btnFullscreen: "btnFullscreen",
			};
		var commandsController = new CommandsController(commandsParameters);

		var whiteBoardController = new WhiteBoardController(whiteBoardModel, commandsController, "canvasWrapper");

		// Instanzia la classe per i menu
		var menusParameters = {
				wrapper: "wrapper",
				menusLayer: "menusLayer",
				btnMenu1: "btnMenu1",
				btnMenu2: "btnMenu2",
				menu1: "menu1",
				menu2: "menu2",
			};
		var menusController = new MenusController(menusParameters);

		// Instanzia le classi per i puntatori
		var pointerModel = new PointerModel(socket, "canvasWrapper");
		var pointerController = new PointerController(pointerModel, "canvasWrapper");

		// Instanzia la classe per la lista utenti
		var usersListModel = new UsersListModel(socket, "usersList");

		// Instanzia le classi per la chat
		var chatParameters = {
				messagebox: "messagebox",
				form: "form",
				inputText: "inputText",
				btnMenu2: "btnMenu2",
				menu2 : "menu2",
			};
		var chatModel = new ChatModel(socket, chatParameters);
		var chatController = new ChatController(chatModel, chatParameters);

		//
		var dialogController = new DialogController("btnDialog");
	}
});