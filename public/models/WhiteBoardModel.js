var WhiteBoardModel = function (socket, canvasId) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabile per identificare la lavagna corrente
	var current = this;

	// Variabili per referenziare gli elementi del DOM
	var canvas = document.getElementById(canvasId);
	canvas.width = canvas.offsetWidth;
	canvas.height = canvas.offsetHeight;

	// Variabile per impostare il contesto di rendering 2D del canvas
	var context = canvas.getContext("2d");
	context.lineCap = "round";
	context.lineJoin = "round";
	
	// Array per memorizzare i tratti
	var sketches = [];
	// Array per memorizzare i tratti annullati
	var undo = [];
	// Variabile per memorizzare il tuo nome
	var clientUserName = "";

	// Variabili che riguardano tipo, colore e spessore del tratto
	var globalCompositeOperation = "source-over";
	var strokeStyle = "#000000";
	var lineWidth = 4;

	/*-----------------*/
	/* PRIVATE METHODS */
	/*-----------------*/
	// Metodo che restitusce l'indice dell'ultimo tratto fatto da un certo autore
	function getLastSketch(author){
		for (var i = sketches.length - 1; i >= 0; i--) {
			if (sketches[i].author == author){
				return i;
			}
		}
		return -1;
	};

	// Metodo che restitusce l'indice dell'ultimo tratto annullato da un certo autore
	function getLastUndo(author){
		for (var i = undo.length - 1; i >= 0; i--) {
			if (undo[i].author == author){
				return i;
			}
		}
		return -1;
	};

	// Metodo privato che cancella e ridisegna il canvas
	function redraw() {
		setTimeout(function(){
			context.clearRect(0, 0, canvas.width, canvas.height);

			// Ridisegna solo i tratti che seguono l'ultima immagine inserita
			var start = 0;
			for (var i = sketches.length - 1; i >= 0 && start != 0; i--) {
				if (sketches[i].image != null) {
					start = sketches.length - 1 - i;
				}
			}

			for (var i = start; i < sketches.length; i++) {

				// Adattatore dimensioni
				var canvasRatio = {
					x: canvas.width/sketches[i].canvasSize.x,
					y: canvas.height/sketches[i].canvasSize.y,
				};

				// Adattatore spessore tratto
				var lineWidthRatio;
				if (window.innerWidth<=640) {
					lineWidthRatio = (1/2);
				}
				else if (window.innerWidth>640 && window.innerWidth<1920) {
					lineWidthRatio = 1;
				}
				else if (window.innerWidth>=1920) {
					lineWidthRatio = 2;
				}
				// Proprietà che riguardano tipo, colore e spessore del tratto
				context.globalCompositeOperation = sketches[i].globalCompositeOperation;
				context.strokeStyle = sketches[i].strokeStyle;
				context.lineWidth = Math.floor(lineWidthRatio*sketches[i].lineWidth);
				
				// Disegna un punto
				if (sketches[i].dot[0].x != null && sketches[i].dot[0].y != null) {
					var dot = sketches[i].dot[0];
					context.beginPath();
					context.fillStyle = sketches[i].strokeStyle;
					context.arc(Math.floor(dot.x*canvasRatio.x), Math.floor(dot.y*canvasRatio.y), Math.floor(lineWidthRatio*sketches[i].lineWidth/2), 0, 2*Math.PI);
					context.fill();
					context.closePath();
				}
				
				// Disegna un tratto
				var stroke = sketches[i].stroke;
				context.beginPath();
				for (var j = 0; j < stroke.length - 1; j++) {
					context.moveTo(Math.floor(stroke[j].x*canvasRatio.x), Math.floor(stroke[j].y*canvasRatio.y));
					context.lineTo(Math.floor(stroke[j + 1].x*canvasRatio.x), Math.floor(stroke[j + 1].y*canvasRatio.y));
				}
				context.closePath();
				context.stroke();

				//Disegna un immagine
				if (sketches[i].image != null) {
					// Variabili per referenziare l'immagine
					var img = document.createElement("img");
					img.src = sketches[i].image;
					context.drawImage(img, 0, 0, canvas.width, canvas.height);
				}
			}
		}, 0);
	};

	// Metodo privato che codifica gli strokes
	function encode(lastSketch, x, y){
		var delta = [];
		delta[0] = sketches[lastSketch].stroke[sketches[lastSketch].stroke.length - 2].x - x;
		delta[1] = sketches[lastSketch].stroke[sketches[lastSketch].stroke.length - 2].y - y;
		var zigzag = [];
		zigzag[0] = (delta[0] << 1) ^ (delta[0] >> 31);
		zigzag[1] = (delta[1] << 1) ^ (delta[1] >> 31);
		return zigzag;
	};

	// Metodo privato che decodifica gli strokes
	function decode(lastSketch, x, y){
		var zigzag = [];
		zigzag[0] = (x >> 1) ^ (-(x & 1));
		zigzag[1] = (y >> 1) ^ (-(y & 1));
		var delta = [];
		delta[0] = sketches[lastSketch].stroke[sketches[lastSketch].stroke.length - 1].x - zigzag[0];
		delta[1] = sketches[lastSketch].stroke[sketches[lastSketch].stroke.length - 1].y - zigzag[1];
		return delta;
	};

	// Metodo privato che controlla la correttezza della struttura dati di un singolo sketch
	function isSketchDataValid(sketch){

		function isStrokeDataValid(stroke){
			if (stroke.length > 1) {
				for (var i = 1; i < stroke.length; i++) {
					if (
						Object.keys(stroke[i]).length === 2
						&&
						stroke[i].hasOwnProperty("x")
						&&
						stroke[i].hasOwnProperty("y")
						&&
						typeof stroke[i].x === "number"
						&&
						typeof stroke[i].y === "number"
						)
					{
						return true;
					}
				};
				return false;
			}
			else{
				return true;
			};	
		};

		return (
			typeof sketch === "object"
			&&
			Object.keys(sketch).length === 8
			&&
			sketch.hasOwnProperty("canvasSize")
			&&
			sketch.hasOwnProperty("author")
			&&
			sketch.hasOwnProperty("dot")
			&&
			sketch.hasOwnProperty("stroke")
			&&
			sketch.hasOwnProperty("globalCompositeOperation")
			&&
			sketch.hasOwnProperty("lineWidth")
			&&
			sketch.hasOwnProperty("strokeStyle")
			&&
			sketch.hasOwnProperty("image")
			&&
			typeof sketch.canvasSize === "object"
			&&
			typeof sketch.author === "string"
			&&
			typeof sketch.dot === "object"
			&&
			typeof sketch.stroke === "object"
			&&
			typeof sketch.globalCompositeOperation === "string"
			&&
			typeof sketch.lineWidth === "number"
			&&
			typeof sketch.strokeStyle === "string"
			&&
			(typeof sketch.image === "string" || sketch.image === null)
			&&
			(sketch.globalCompositeOperation === "source-over"
				||
				sketch.globalCompositeOperation === "source-in"
				||
				sketch.globalCompositeOperation === "source-out"
				||
				sketch.globalCompositeOperation === "source-atop"
				||
				sketch.globalCompositeOperation === "destination-over"
				||
				sketch.globalCompositeOperation === "destination-in"
				||
				sketch.globalCompositeOperation === "destination-out"
				||
				sketch.globalCompositeOperation === "destination-atop"
				||
				sketch.globalCompositeOperation === "lighter"
				||
				sketch.globalCompositeOperation === "copy"
				||
				sketch.globalCompositeOperation === "xor"
				||
				sketch.globalCompositeOperation === "multiply"
				||
				sketch.globalCompositeOperation === "screen"
				||
				sketch.globalCompositeOperation === "overlay"
				||
				sketch.globalCompositeOperation === "darken"
				||
				sketch.globalCompositeOperation === "lighten"
				||
				sketch.globalCompositeOperation === "color-dodge"
				||
				sketch.globalCompositeOperation === "color-burn"
				||
				sketch.globalCompositeOperation === "hard-light"
				||
				sketch.globalCompositeOperation === "soft-light"
				||
				sketch.globalCompositeOperation === "difference"
				||
				sketch.globalCompositeOperation === "exclusion"
				||
				sketch.globalCompositeOperation === "hue"
				||
				sketch.globalCompositeOperation === "saturation"
				||
				sketch.globalCompositeOperation === "color"
				||
				sketch.globalCompositeOperation === "luminosity")
			&&
			(sketch.strokeStyle).match(/(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i)
			&&
			Object.keys(sketch.canvasSize).length === 2
			&&
			sketch.canvasSize.hasOwnProperty("x")
			&&
			sketch.canvasSize.hasOwnProperty("y")
			&&
			(typeof sketch.canvasSize.x === "number" || (typeof sketch.canvasSize.x === "object" && sketch.canvasSize.x === null))
			&&
			(typeof sketch.canvasSize.y === "number" || (typeof sketch.canvasSize.y === "object" && sketch.canvasSize.y === null))
			&&
			sketch.dot.length === 1
			&&
			typeof sketch.dot[0] === "object"
			&&
			sketch.stroke.length >= 1
			&&
			typeof sketch.stroke[0] === "object"
			&&
			Object.keys(sketch.dot[0]).length === 2
			&&
			Object.keys(sketch.stroke[0]).length === 2
			&&
			sketch.dot[0].hasOwnProperty("x")
			&&
			sketch.dot[0].hasOwnProperty("y")
			&&
			sketch.stroke[0].hasOwnProperty("x")
			&&
			sketch.stroke[0].hasOwnProperty("y")
			&&
			(typeof sketch.dot[0].x === "number" || (typeof sketch.dot[0].x === "object" && sketch.dot[0].x === null))
			&&
			(typeof sketch.dot[0].y === "number" || (typeof sketch.dot[0].y === "object" && sketch.dot[0].y === null))
			&&
			(typeof sketch.stroke[0].x === "number" || (typeof sketch.stroke[0].x === "object" && sketch.stroke[0].x === null))
			&&
			(typeof sketch.stroke[0].y === "number" || (typeof sketch.stroke[0].y === "object" && sketch.stroke[0].y === null))
			&&
			isStrokeDataValid(sketch.stroke) === true
		);
	};

	// Metodo privato che ripulisce un singolo sketch dai dati ridondanti
	function purgeSketch(sketch) {
		
		var purgedSketch = ({
			canvasSize: sketch.canvasSize,
			author: sketch.author,
			dot: [{
				x: sketch.dot[0].x,
				y: sketch.dot[0].y
			}],
			stroke: [{
				x: sketch.stroke[0].x,
				y: sketch.stroke[0].y
			}],
			globalCompositeOperation : sketch.globalCompositeOperation,
			lineWidth : sketch.lineWidth,
			strokeStyle : sketch.strokeStyle,
			image: sketch.image,
		});
		
		for (var i = 0; i < sketch.stroke.length; i++) {
			if (i == 0) {
				purgedSketch.stroke.push({
					x: sketch.stroke[i].x,
					y: sketch.stroke[i].y
				});
			}
			else if (!(sketch.stroke[i].x == sketch.stroke[i - 1].x && sketch.stroke[i].y == sketch.stroke[i - 1].y)) {
				purgedSketch.stroke.push({
					x: sketch.stroke[i].x,
					y: sketch.stroke[i].y
				});
			}
		}
		
		return purgedSketch;
	}

	// Metodo privato che ripulisce la struttura dati della lavagna dai dati ridondanti
	function purgeSketches() {
		var purgedSketches = [];

		for (var i = 0; i < sketches.length; i++) {

			purgedSketches.push(purgeSketch(sketches[i]));

		}

		sketches = purgedSketches;
	}

	/*----------------*/
	/* PUBLIC METHODS */
	/*----------------*/
	// Metodo pubblico che aggiunge un punto alla struttura dati della whiteboard
	this.addSketchWithDot = function (cursor) {
		
		// Svuota l'array undo
		undo = [];

		// Inserisce un elemento in coda all'array sketches
		sketches.push({
			canvasSize: {x: canvas.width, y: canvas.height},
			author: clientUserName,
			dot : [],
			stroke : [],
			globalCompositeOperation : current.globalCompositeOperation,
			lineWidth : current.lineWidth,
			strokeStyle : current.strokeStyle,
			image : null,
		});

		// Inserisce le coordiante di un singolo punto
		sketches[sketches.length - 1].dot.push({
			x: cursor.x,
			y: cursor.y
		});

		// Inserisce le coordinate come punto di inizio di un tratto
		sketches[sketches.length - 1].stroke.push({
			x: cursor.x,
			y: cursor.y
		});
		
		redraw();

		// Comunica al server l'evento
		socket.emit("ClientEventMouseDown", sketches[sketches.length - 1]);
	};

	// Metodo pubblico che aggiunge un punto alla struttura dati della whiteboard
	this.addSketchWithoutDot = function (cursor) {
		
		// Svuota l'array undo
		undo = [];

		// Inserisce un elemento in coda all'array sketches
		sketches.push({
			canvasSize: {x: canvas.width, y: canvas.height},
			author: clientUserName,
			dot : [],
			stroke : [],
			globalCompositeOperation : current.globalCompositeOperation,
			lineWidth : current.lineWidth,
			strokeStyle : current.strokeStyle,
			image : null,
		});

		// Inserisce le coordiante di un singolo punto
		sketches[sketches.length - 1].dot.push({
			x: null,
			y: null
		});

		// Inserisce le coordinate come punto di inizio di un tratto
		sketches[sketches.length - 1].stroke.push({
			x: cursor.x,
			y: cursor.y
		});
		
		redraw();

		// Comunica al server l'evento
		socket.emit("ClientEventMouseDown", sketches[sketches.length - 1]);
	};

	// Metodo pubblico che aggiunge un tratto alla struttura dati della whiteboard
	this.addStroke = function (cursor) {
		
		// Metodo che restitusce l'indice dell'ultimo tratto fatto dal client
		var lastSketch = getLastSketch(clientUserName);

		// se il punto rilevato è ridondante non lo processa
		if (!(sketches[lastSketch].stroke[sketches[lastSketch].stroke.length - 1].x == cursor.x &&
			sketches[lastSketch].stroke[sketches[lastSketch].stroke.length - 1].y == cursor.y)) {

			// Aggiunge la coordianta in coda all'array del tratto
			sketches[lastSketch].stroke.push({
				x: cursor.x,
				y: cursor.y
			});

			redraw();

			// Comunica al server l'evento
			var encoded = encode(lastSketch, cursor.x, cursor.y);
			socket.emit("ClientEventMouseMoveStroke", encoded);
		}
	};

	// Metodo pubblico che aggiunge un tratto alla struttura dati della whiteboard
	this.addLine = function (cursor) {
		
		// Metodo che restitusce l'indice dell'ultimo tratto fatto dal client
		var lastSketch = getLastSketch(clientUserName);

		// se il punto rilevato è ridondante non lo processa
		if (!(sketches[lastSketch].stroke[0].x == cursor.x &&
			sketches[lastSketch].stroke[0].y == cursor.y)) {

			sketches[lastSketch].stroke[1] = {
				x: cursor.x,
				y: cursor.y
			};

			redraw();

			socket.emit("ClientEventMouseMoveLine", cursor);
		}
	};

	// Metodo pubblico che aggiunge un tratto alla struttura dati della whiteboard
	this.addRect = function (cursor) {
		
		// Metodo che restitusce l'indice dell'ultimo tratto fatto dal client
		var lastSketch = getLastSketch(clientUserName);

		// se il punto rilevato è ridondante non lo processa
		if (!(sketches[lastSketch].stroke[0].x == cursor.x &&
			sketches[lastSketch].stroke[0].y == cursor.y)) {

			sketches[lastSketch].stroke[1] = {
				x: cursor.x,
				y: sketches[lastSketch].stroke[0].y
			};

			sketches[lastSketch].stroke[2] = {
				x: cursor.x,
				y: cursor.y
			};

			sketches[lastSketch].stroke[3] = {
				x: sketches[lastSketch].stroke[0].x,
				y: cursor.y
			};

			sketches[lastSketch].stroke[4] = {
				x: sketches[lastSketch].stroke[0].x,
				y: sketches[lastSketch].stroke[0].y
			};

			redraw();

			socket.emit("ClientEventMouseMoveRect", cursor);
		}
	};

	// Metodo pubblico che aggiunge un tratto alla struttura dati della whiteboard
	this.addEllipse = function (mouseDownCursor, cursor) {
		
		// Metodo che restitusce l'indice dell'ultimo tratto fatto dal client
		var lastSketch = getLastSketch(clientUserName);

		//sketches[lastSketch].dot[0].x = null;
		//sketches[lastSketch].dot[0].y = null;

		var sides = 60;

		var x1 = mouseDownCursor.x;
		var y1 = mouseDownCursor.y;

		var cx = (x1 + cursor.x)/2;
		var cy = (y1 + cursor.y)/2;

		var a = (Math.abs(x1 - cursor.x))/2;
		var b = (Math.abs(y1 - cursor.y))/2;		

		// se il punto rilevato è ridondante non lo processa
		if (!(x1 == cursor.x && y1 == cursor.y)) {

			for (var i = 0; i < sides; i++) {
			var theta = (i/sides)*2*Math.PI;
				sketches[lastSketch].stroke[i] = ({
					x: Math.floor((cx + a*Math.cos(theta))*100)/100,
					y: Math.floor((cy + b*Math.sin(theta))*100)/100
				});
			};

			sketches[lastSketch].stroke[sides] = ({
				x: Math.floor((cx + a*Math.cos(0))*100)/100,
				y: Math.floor((cy + b*Math.sin(0))*100)/100
			});

			redraw();

			socket.emit("ClientEventMouseMoveEllipse", {mouseDownCursor: mouseDownCursor, cursor: cursor});
		}
	};

	// Metodo pubblico per cancellare l'intero canvas
	this.new = function() {
		sketches = [];
		undo = []; /*cancella cronologia undo*/
		
		redraw();
		
		// Comunica al server l'evento
		socket.emit("ClientEventNew");
	};

	// Metodo pubblico per aprire uno sketch nel canvas
	this.open = function(file) {
		var reader = new FileReader();
		var content;
		var json;

		if (typeof FileReader !== "undefined" && file.type.indexOf("image") != -1) {
			reader.onload = function (event) {
				content = event.target.result;
				sketches.push({
					canvasSize: {x: null, y: null},
					author: clientUserName,
					dot : [{
						x : null,
						y : null
					}],
					stroke : [{
						x : null,
						y : null
					}],
					globalCompositeOperation : "source-over",
					lineWidth : 4,
					strokeStyle : "#000000",
					image : content,
				});
				
				/*Esperimento per rendere il primo inserimento dell'immagine responsive*/
				var img = new Image();
				img.onload = function(){
					context.drawImage(img, 0, 0, canvas.width, canvas.height);
				}
				img.src = sketches[sketches.length - 1].image;
				/**/

				redraw();
				
				// Comunica al server l'evento
				socket.emit("ClientEventOpen", sketches[sketches.length - 1]);
			};
			reader.readAsDataURL(file);
		}

		else {
			reader.onload = function (event) {  
				content = event.target.result;
				var isValid = true;
				// Prova ad analizzare il JSON
				try{
					json = JSON.parse(content);
					var sketch;
					for (var i = 0; i < json.length; i++) {
						sketch = json[i];
						if (isSketchDataValid(sketch)) {
							
							// Chi apre un file diventa autore dei tratti contenuti
							sketch.author = clientUserName;
							sketches.push(purgeSketch(sketch));
						
							// Comunica al server l'evento
							socket.emit("ClientEventOpen", sketches[sketches.length - 1]);
						}
						else{
							isValid = false;
						}
					}
				}
				catch(error){
					isValid = false;
				}
				finally{
					if (isValid == false) {
						alert("File corrupted");
					};
				}
				
				redraw();
				
			};
			reader.readAsText(file);
		}
    };

    // Metodo pubblico per salvare uno sketch nel canvas
    this.save = function(link, filename) {
		makeTextFile = function (text) {
			var data = new Blob([text], {type: "application/json"});
			textFile = window.URL.createObjectURL(data);
			return textFile;
		};

		purgeSketches();

    	link.href = makeTextFile(JSON.stringify(sketches));
    	link.download = filename;
    };

    // Metodo pubblico per scaricare l'immagine del canvas
	this.export = function(link, filename) {
			link.href = canvas.toDataURL();
			link.download = filename;
	};

	// Metodo pubblico per annullare
	this.undo = function() {
		var lastSketch = getLastSketch(clientUserName);
		if (lastSketch >= 0) {
			undo.push(sketches[lastSketch]);
			sketches.splice(lastSketch,1);
			
			redraw();
			
			// Comunica al server l'evento
			socket.emit("ClientEventUndo");
		}
	};

	// Metodo pubblico per ripetere
	this.redo = function() {
		var lastUndo = getLastUndo(clientUserName);
		if (lastUndo >= 0) {
			sketches.push(undo[lastUndo]);

			// Comunica al server l'evento
			socket.emit("ClientEventRedo", undo[lastUndo]);

			undo.splice(lastUndo,1);

			redraw();
		}
	};
	// Metodo pubblico per aggiornare le dimensioni del canvas
	this.updateCanvasSize = function(){
		canvas.width = canvas.offsetWidth;
		canvas.height = canvas.offsetHeight;
		context.lineCap = "round";
		context.lineJoin = "round";
		
		redraw();
	};

	// Metodo pubblico per configurare la dinamica di sovrepposizione del colore del tratto
	this.setGlobalCompositeOperation = function (globalCompositeOperation){
		current.globalCompositeOperation = globalCompositeOperation;
	};

	// Metodo pubblico per configurare il colore del tratto
	this.setStrokeStyle = function (strokeStyle){
		current.strokeStyle = strokeStyle;
	};

	// Metodo pubblico per configurare lo spessore del tratto
	this.setLineWidth = function (lineWidth){
		current.lineWidth = lineWidth;
	};

	// Metodo pubblico per ottenere l'id del canvas che fa da base per la lavagna
	this.getCanvasId = function (){
		return canvasId;
	};

	/*-----------------------------*/
	/* SERVER LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Quando il server emette l'evento "ServerEventConnected"
	socket.on("ServerEventInRoom", function(data) {
		clientUserName = data.userName;
		sketches = data.sketches;
		undo = data.undo;
		
		redraw();
	});

	// Quando il server emette l'evento
	socket.on("ServerEventMouseDown", function(data) {
		sketches.push(data.content);
		
		redraw();
		
	});

	// Quando il server emette l'evento
	socket.on("ServerEventMouseMoveStroke", function(data) {
		var lastSketch = getLastSketch(data.userName);
		/*
		sketches[lastSketch].stroke.push(data.content);
		*/
		
		var decoded = decode(lastSketch, data.content[0], data.content[1]);
		sketches[lastSketch].stroke.push({
			x: decoded[0],
			y: decoded[1]
		});
		
		redraw();
		
	});

	// Quando il server emette l'evento
	socket.on("ServerEventMouseMoveLine", function(data) {
		var lastSketch = getLastSketch(data.userName);
		
		sketches[lastSketch].stroke[1] = data.content;
		
		redraw();
		
	});

	// Quando il server emette l'evento
	socket.on("ServerEventMouseMoveRect", function(data) {
		var lastSketch = getLastSketch(data.userName);

		sketches[lastSketch].stroke[1] = {
			x: data.content.x,
			y: sketches[lastSketch].stroke[0].y
		};

		sketches[lastSketch].stroke[2] = {
			x: data.content.x,
			y: data.content.y
		};

		sketches[lastSketch].stroke[3] = {
			x: sketches[lastSketch].stroke[0].x,
			y: data.content.y
		};

		sketches[lastSketch].stroke[4] = {
			x: sketches[lastSketch].stroke[0].x,
			y: sketches[lastSketch].stroke[0].y
		};
		
		redraw();
		
	});

	// Quando il server emette l'evento
	socket.on("ServerEventMouseMoveEllipse", function(data) {
		var lastSketch = getLastSketch(data.userName);

		sketches[lastSketch].dot[0].x = null;
		sketches[lastSketch].dot[0].y = null;

		var sides = 60;

		var x1 = data.content.mouseDownCursor.x;
		var y1 = data.content.mouseDownCursor.y;

		var cx = (x1 + data.content.cursor.x)/2;
		var cy = (y1 + data.content.cursor.y)/2;

		var a = (Math.abs(x1 - data.content.cursor.x))/2;
		var b = (Math.abs(y1 - data.content.cursor.y))/2;		

		for (var i = 0; i < sides; i++) {
			var theta = (i/sides)*2*Math.PI;
			sketches[lastSketch].stroke[i] = ({
				x: Math.floor((cx + a*Math.cos(theta))*100)/100,
				y: Math.floor((cy + b*Math.sin(theta))*100)/100
			});
		};

		sketches[lastSketch].stroke[sides] = ({
			x: Math.floor((cx + a*Math.cos(0))*100)/100,
			y: Math.floor((cy + b*Math.sin(0))*100)/100
		});

		redraw();
	});

	// Quando il server emette l'evento
	socket.on("ServerEventNew", function(data) {
		sketches = [];
		undo = []; /*cancella cronologia undo*/
		
		redraw();
		
	});

	// Quando il server emette l'evento
	socket.on("ServerEventOpen", function(data) {
		sketches.push(data.content);
		
		/*Esperimento per rendere il primo inserimento dell'immagine responsive*/
		if (sketches[sketches.length - 1].image != null) {
			var img = new Image();
			img.onload = function(){
				context.drawImage(img, 0, 0, canvas.width, canvas.height);
			}
			img.src = sketches[sketches.length - 1].image;
		}
		/**/

		redraw();
		
	});

	// Quando il server emette l'evento
	socket.on("ServerEventUndo", function(data) {
		var lastSketch = getLastSketch(data.userName);
		if (lastSketch >= 0) {
			undo.push(sketches[lastSketch]);
			sketches.splice(lastSketch,1);
			
			redraw();
			
		}
	});

	// Quando il server emette l'evento
	socket.on("ServerEventRedo", function(data) {
		sketches.push(data.content);
		
		redraw();
		
	});
};