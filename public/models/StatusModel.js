var StatusModel = function (socket, statusId) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabili per referenziare gli elementi del DOM
	var status = document.getElementById(statusId);

	/*-----------------------------*/
	/* SERVER LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Quando il server emette l'evento "ServerEventConnected"
	socket.on("ServerEventConnected", function() {
		status.innerHTML = "Not Logged";
	});
	// Quando il server emette l'evento "ServerEventLogResponse"
	socket.on("ServerEventInLobby", function(data) {
		var yourUserName = data.userName;
		status.innerHTML = "Logged as \"" + yourUserName + "\"";
	});
	// Quando il server emette l'evento "ServerEventInRoom"
	socket.on("ServerEventInRoom", function(data) {
		var yourUserName = data.userName;
		var yourRoomName = data.roomName;
		status.innerHTML = "Logged as \"" + yourUserName + "\" in \"" + yourRoomName + "\"";
	});
};