var AuthenticationModel = function(socket, parameters) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabili per referenziare gli elementi del DOM
	var btnLogIn = document.getElementById(parameters.btnLogIn);
	var btnCreateRoom = document.getElementById(parameters.btnCreateRoom);
	var btnJoinRoom = document.getElementById(parameters.btnJoinRoom);
	var btnLogOut = document.getElementById(parameters.btnLogOut);

	// Variabile che definisce il nome e la durata dei cookie
	var cookieExpireInSeconds = 30*60;
	var cookieName = "schetchy";

	/*-----------------*/
	/* PRIVATE METHODS */
	/*-----------------*/
	function setCookie(cookieName, cookieValue, expireInSeconds) {
		var expireDate = new Date();
		expireDate.setTime(expireDate.getTime() + (expireInSeconds*1000));
		var expires = "expires=" + expireDate.toGMTString();
		document.cookie = cookieName+"="+cookieValue+"; "+expires;
	}

	function getCookie(cookieName) {
		var name = cookieName + "=";
		var cookieArray = document.cookie.split(';');
		for(var i=0; i<cookieArray.length; i++) {
			var c = cookieArray[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return null;
	}

	function deleteCookie(cookieName) {
		document.cookie = cookieName + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
	}
	function isThereCookie(cookieName) {
		if (getCookie(cookieName) != null) {
			return true;
		}
		else {
			return false;
		}
	}

	function isThereCookieToLog(cookieName) {
		if(isThereCookie(cookieName) == true &&
			JSON.parse(getCookie(cookieName)).userName != null &&
			JSON.parse(getCookie(cookieName)).userPassword != null &&
			JSON.parse(getCookie(cookieName)).roomName == null &&
			JSON.parse(getCookie(cookieName)).roomPassword == null
		){
			return true;
		}
		else {
			return false;
		}
	}

	function isThereCookieToJoinRoom(cookieName) {
		if(isThereCookie(cookieName) == true &&
			JSON.parse(getCookie(cookieName)).userName != null &&
			JSON.parse(getCookie(cookieName)).userPassword != null &&
			JSON.parse(getCookie(cookieName)).roomName != null &&
			JSON.parse(getCookie(cookieName)).roomPassword != null
		){
			return true;
		}
		else {
			return false;
		}
	}

	// Se ci sono cookie validi li usa
	if (isThereCookieToLog(cookieName) == true) {
		userName = JSON.parse(getCookie(cookieName)).userName;
		userPassword = JSON.parse(getCookie(cookieName)).userPassword;
		roomName = null;
		roomPassword = null;
	}
	else if (isThereCookieToJoinRoom(cookieName) == true) {
		userName = JSON.parse(getCookie(cookieName)).userName;
		userPassword = JSON.parse(getCookie(cookieName)).userPassword;
		roomName = JSON.parse(getCookie(cookieName)).roomName;
		roomPassword = JSON.parse(getCookie(cookieName)).roomPassword;
	}
	/*----------------*/
	/* PUBLIC METHODS */
	/*----------------*/
	// Metodo pubblico che emette una richiesta, di login, al server
	this.emitLogRequest = function (userName, userPassword) {
		socket.emit("ClientEventLogRequest", {
			userName: userName,
			userPassword: userPassword,
		});
	};
	// Metodo pubblico che emette una richiesta, per creare una stanza, al server
	this.emitCreateRoomRequest = function (roomName, roomPassword) {
		socket.emit("ClientEventCreateRoomRequest", {
				roomName: roomName,
				roomPassword: roomPassword,
			});
	};
	// Metodo pubblico che emette una richiesta, per unirsi a una stanza, al server
	this.emitJoinRoomRequest = function (roomName, roomPassword) {
		socket.emit("ClientEventJoinRoomRequest", {
				roomName: roomName,
				roomPassword: roomPassword,
			});
	};
	// Metodo pubblico che emette un evento di logout al server
	this.emitLogOut = function () {
		deleteCookie(cookieName);
		socket.emit("ClientEventLogOut");
	};

	/*-----------------------------*/
	/* SERVER LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Quando il server emette l'evento "ServerEventConnected"
	socket.on("ServerEventConnected", function() {
		// Se sono presenti cookie validi vengono usati
		if(isThereCookieToLog(cookieName) == true || isThereCookieToJoinRoom(cookieName) == true){
			// Comunica al server l'evento
			socket.emit("ClientEventLogRequest", {
				userName: userName,
				userPassword: userPassword,
			});
		}
	});
	
	// Quando il server emette l'evento "ServerEventLogResponse"
	socket.on("ServerEventLogResponse", function(data) {
		if (data == "accepted") {
			if(isThereCookieToJoinRoom(cookieName) == true){
				// Comunica al server l'evento
				socket.emit("ClientEventJoinRoomRequest", {
					roomName: roomName,
					roomPassword: roomPassword,
				});
				var cookieData = {
					userName: userName,
					userPassword: userPassword,
					roomName: roomName,
					roomPassword: roomPassword,
				};
			}
			else {
				var cookieData = {
					userName: userName,
					userPassword: userPassword,
					roomName: null,
					roomPassword: null,
				};
			}
			setCookie(cookieName, JSON.stringify(cookieData), cookieExpireInSeconds);
			
			btnLogIn.className = "removed";
			btnCreateRoom.className = "btn";
			btnJoinRoom.className = "btn";
			btnLogOut.className = "btn";
		}
		else{
			alert("Username already taken, please choose another one.");
			deleteCookie(cookieName);
			
			btnLogIn.className = "btn";
			btnCreateRoom.className = "removed";
			btnJoinRoom.className = "removed";
			btnLogOut.className = "removed";
		}
	});
	
	// Quando il server emette l'evento
	socket.on("ServerEventCreateRoomResponse", function(data) {
		if (data == "accepted") {
			var cookieData = {
				userName: userName,
				userPassword: userPassword,
				roomName: roomName,
				roomPassword: roomPassword,
			};
			
			btnLogIn.className = "removed";
			btnCreateRoom.className = "removed";
			btnJoinRoom.className = "removed";
			btnLogOut.className = "btn";
		}
		else{
			alert("Room name already taken, please choose another one.");
			var cookieData = {
				userName: userName,
				userPassword: userPassword,
				roomName: null,
				roomPassword: null,
			};
			
			btnLogIn.className = "removed";
			btnCreateRoom.className = "btn";
			btnJoinRoom.className = "btn";
			btnLogOut.className = "btn";
		}
		setCookie(cookieName, JSON.stringify(cookieData), cookieExpireInSeconds);
	});

	// Quando il server emette l'evento
	socket.on("ServerEventJoinRoomResponse", function(data) {
		if (data == "accepted") {
			var cookieData = {
				userName: userName,
				userPassword: userPassword,
				roomName: roomName,
				roomPassword: roomPassword,
			};
			
			btnLogIn.className = "removed";
			btnCreateRoom.className = "removed";
			btnJoinRoom.className = "removed";
			btnLogOut.className = "btn";
		}
		else{
			alert("Incorrect room name or password.");
			var cookieData = {
				userName: userName,
				userPassword: userPassword,
				roomName: null,
				roomPassword: null,
			};
			
			btnLogIn.className = "removed";
			btnCreateRoom.className = "btn";
			btnJoinRoom.className = "btn";
			btnLogOut.className = "btn";
		}
		setCookie(cookieName, JSON.stringify(cookieData), cookieExpireInSeconds);
	});
};