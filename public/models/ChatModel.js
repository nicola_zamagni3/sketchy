var ChatModel = function (socket, parameters) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabili per referenziare gli elementi del DOM
	var messagebox = document.getElementById(parameters.messagebox);
	var btnMenu2 = document.getElementById(parameters.btnMenu2);
	var menu2 = document.getElementById(parameters.menu2);

	var clientUserName = "";
	var newmessage = new Audio("./views/assets/sounds/newmessage.mp3");

	/*-----------------*/
	/* PRIVATE METHODS */
	/*-----------------*/
	// Metodo privato che aggiunge un messaggio alla chat
	function addChatMessage(data){
		var message = document.createElement("P");
		var text = document.createTextNode(data);
		message.appendChild(text);
		messagebox.appendChild(message);
		messagebox.scrollTop = messagebox.scrollHeight;
	}

	/*----------------*/
	/* PUBLIC METHODS */
	/*----------------*/
	// Metodo pubblico che invia un messaggio al server
	this.emitNewMessage = function(text) {
		socket.emit("ClientEventNewMessage", text);
	};

	/*-----------------------------*/
	/* SERVER LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Quando il server emette l'evento "ServerEventInRoom"
	socket.on("ServerEventInRoom", function(data) {
		clientUserName = data.userName;
		var messages = data.messages;

		for (var i = 0; i < messages.length; i++) {
			addChatMessage("" + messages[i].author + ": " +messages[i].message);
		}
	});
	// Quando il server emette l'evento "ServerEventUserJoined"
	socket.on("ServerEventUserJoined", function(data) {
		//addChatMessage("user " + data.userName + " joined");
	});

	// Quando il server emette l'evento "ServerEventNewMessage"
	socket.on("ServerEventNewMessage", function(data) {
		addChatMessage("" + data.userName + ": " +data.message);
		if (clientUserName != data.userName) {
			if (menu2.className == "slideOut") {
				newmessage.play();
				btnMenu2.style.backgroundImage = "url(\"./views/assets/icons/notificationAlert.svg\")";
			}
		}
	});

	// Quando il server emette l'evento "ServerEventUserleft"
	socket.on("ServerEventUserleft", function(data) {
		//addChatMessage("user " + data.userName + " left");
	});
};