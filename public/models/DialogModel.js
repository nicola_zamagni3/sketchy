var DialogModel = function(parameters){
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabile per identificare il dialog corrente
	var current = this;

	// Titolo del dialog
	var title = null;
	// Contenuto testuale del dialog
	var content = null;

	// Placeholder inseriti nell'InputText del dialog
	var inputText1.placeholder = null;
	var inputText2.placeholder = null;

	// Valori inseriti nell'InputText del dialog
	var inputText1.value = null;
	var inputText2.value = null;
	
	// Etichetta dei bottoni del dialog
	var positiveButton.label = null;
	var negativeButton.label = null;

	// Stato dei bottoni del dialog (enabled/disabled)
	var positiveButton.status = null;
	var negativeButton.status = null;
	
	/*----------------*/
	/* PUBLIC METHODS */
	/*----------------*/
	// Metodo pubblico per configurare il titolo del dialog
	this.setTitle = function (title){
		current.title = title;
	};

	// Metodo pubblico per configurare il contenuto testuale del dialog
	this.setContent = function (content){
		current.content = content;
	};

	// Metodo pubblico per configurare il placehoder nell'InputText del dialog
	this.setInputTextPlaceholder = function (inputText, placeholder){
		current.inputText.placeholder = placeholder;
	};

	// Metodo pubblico per configurare il valore dell'inputText del dialog
	this.setInputTextValue = function (inputText, value){
		current.inputText.value = value;
	};

	// Metodo pubblico per ottenere il valore dell'inputText del dialog
	this.getInputTextValue = function (inputText){
		return inputText.value;
	};

	// Metodo pubblico per configurare l'etichetta di un bottone del dialog
	this.setButtonLabel = function (button, label){
		current.button.label = label;
	};

	// Metodo pubblico per configurare lo stato di un bottone del dialog (enabled/disabled)
	this.setButtonStatus = function (button, status){
		current.button.status = status;
	};

	// Metodo pubblico per ottenere lo stato di un bottone del dialog (enabled/disabled)
	this.getButtonStatus = function (button){
		return button.status;
	};

	// Crea dinamicamente elementi html
	function createHtmlElement(elementType, elementFather){
		var element = document.createElement(elementType);
		elementFather.appendChild(element);
		return element;
	}
	// Rimuove elementi html
	function removeHtmlElement(element){
		element.parentNode.removeChild(element);
	}

	function createAlert() {}:
	function createConfirm() {};
	function createPrompt1() {};
	function createPrompt2() {};
};