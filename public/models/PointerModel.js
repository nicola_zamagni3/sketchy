var PointerModel = function (socket, canvasWrapperId) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	/// Variabili per referenziare gli elementi del DOM
	var canvasWrapper = document.getElementById(canvasWrapperId);
	canvasWrapper.width = canvas.offsetWidth;
	canvasWrapper.height = canvas.offsetHeight;

	/*-----------------*/
	/* PRIVATE METHODS */
	/*-----------------*/
	// Metodo privato che genera un cursore con userName
	function makePointer(userName){

		var div = document.createElement("div");
		div.className = "pointer";
		div.id = userName + "pointer";
		canvasWrapper.appendChild(div);

		var img = document.createElement("img");
		img.setAttribute("draggable", "false");
		img.src = "./views/assets/cursors/pointer.svg";
		div.appendChild(img);

		var p = document.createElement("p");
		var text = document.createTextNode(userName);
		p.appendChild(text);
		div.appendChild(p);
	}

	/*----------------*/
	/* PUBLIC METHODS */
	/*----------------*/
	// Metodo pubblico che invia al server la posizione del cursore
	this.emitCursorMove = function (cursor) {
		socket.emit("ClientEventCursorMove",
			{canvasSize: {x: canvasWrapper.width, y: canvasWrapper.height},
			cursor: cursor});
	};

	// Metodo pubblico per aggiornare le dimensioni del canvasWrapper
	this.updateCanvasSize = function(){
		canvasWrapper.width = canvas.offsetWidth;
		canvasWrapper.height = canvas.offsetHeight;
	};

	/*-----------------------------*/
	/* SERVER LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Quando il server emette l'evento "ServerEventUserJoined"
	socket.on("ServerEventUserJoined", function(data) {
		//aggiungi pointer
		var userName = data.userName;
		makePointer(userName);
		var pointer = document.getElementById(userName + "pointer");
		pointer.style.left = "" + (canvasWrapper.getBoundingClientRect().left) + "px";
		pointer.style.top = "" + (canvasWrapper.getBoundingClientRect().top) + "px";
	});

	// Quando il server emette l'evento
	socket.on("ServerEventCursorMove", function(data) {
		var userName = data.userName;
		if (!(!!document.getElementById(userName + "pointer"))){
			makePointer(userName);
		}
		var pointer = document.getElementById(userName + "pointer");
		
		// Adattatore dimensioni
		var ratio = {
			x: canvasWrapper.width/data.content.canvasSize.x,
			y: canvasWrapper.height/data.content.canvasSize.y,
		};

		if ((data.content.cursor.x*ratio.x >= 0 && data.content.cursor.x*ratio.x <= canvasWrapper.width) && (data.content.cursor.y*ratio.y >= 0 && data.content.cursor.y*ratio.y <= canvasWrapper.height)) {
			pointer.style.left = "" + (data.content.cursor.x*ratio.x) + "px";
			pointer.style.top = "" + (data.content.cursor.y*ratio.y) + "px";
		}
	});

	// Quando il server emette l'evento "ServerEventUserleft"
	socket.on("ServerEventUserleft", function(data) {
		//togli pointer
		var userName = data.userName;
		if (document.getElementById(userName + "pointer")) {
			document.getElementById(userName + "pointer").remove();
		}
	});
};