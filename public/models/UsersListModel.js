var UsersListModel = function (socket, usersListId) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabili per referenziare gli elementi del DOM
	var usersList = document.getElementById(usersListId);

	/*-----------------*/
	/* PRIVATE METHODS */
	/*-----------------*/
	// Metodo privato che aggiunge userName
	function addUser(userName){
		var div = document.createElement("div");
		div.id = userName + "user";
		usersList.appendChild(div);

		var img = document.createElement("img");
		img.src = "./views/assets/icons/online.svg";
		div.appendChild(img);

		var span = document.createElement("span");
		var text = document.createTextNode(userName);
		span.appendChild(text);
		div.appendChild(span);
	}

	/*-----------------------------*/
	/* SERVER LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Quando il server emette l'evento "ServerEventInRoom"
	socket.on("ServerEventInRoom", function(data) {
		var yourUserName = data.userName;
		var users = data.users;

		for (var i = 0; i < users.length; i++) {
			if (users[i] != yourUserName){
				addUser(users[i]);
			}
		}
	});

	// Quando il server emette l'evento "ServerEventUserJoined"
	socket.on("ServerEventUserJoined", function(data) {
		// Aggiunge user
		var userName = data.userName;
		addUser(userName);
	});

	// Quando il server emette l'evento "ServerEventUserleft"
	socket.on("ServerEventUserleft", function(data) {
		// Rimuove user
		var userName = data.userName;
		if (document.getElementById(userName + "user")) {
			document.getElementById(userName + "user").remove();
		}
	});
};