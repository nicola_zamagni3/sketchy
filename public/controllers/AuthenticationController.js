var AuthenticationController = function(authenticationModel, parameters) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabili per referenziare gli elementi del DOM
	var btnLogIn = document.getElementById(parameters.btnLogIn);
	var btnCreateRoom = document.getElementById(parameters.btnCreateRoom);
	var btnJoinRoom = document.getElementById(parameters.btnJoinRoom);
	var btnLogOut = document.getElementById(parameters.btnLogOut);

	/*-----------------*/
	/* PRIVATE METHODS */
	/*-----------------*/
	function logInPrompt(){
		userName = prompt("Enter your username","");
		userPassword = prompt("Enter your password","");
		if (userName != null && (userName).match(/^[a-zA-Z0-9_-]{3,15}$/)) {
			// Comunica al server l'evento
			authenticationModel.emitLogRequest(userName, userPassword);
		}
		else{
			alert("The username must be, 3 to 15 characters long\nwith any character, digit or special symbol “_-” only.");
		}
	}

	function roomPrompt(action){
		roomName = prompt("Enter room name","");
		roomPassword = prompt("Enter room password","");
		if (action == "create" && roomName != null && (roomName).match(/^[a-zA-Z0-9_-]{3,15}$/)) {
			// Comunica al server l'evento
			authenticationModel.emitCreateRoomRequest(roomName, roomPassword);
		}
		else if (action == "join" && roomName != null && (roomName).match(/^[a-zA-Z0-9_-]{3,15}$/)) {
			// Comunica al server l'evento
			authenticationModel.emitJoinRoomRequest(roomName, roomPassword);
		}
		else{
			alert("The room name must be, 3 to 15 characters long\nwith any character, digit or special symbol “_-” only.");
		}
	}

	function logOutConfirm(){
		logOut = confirm("Are you sure?");
		if (logOut == true) {
			// Comunica al server l'evento
			authenticationModel.emitLogOut();
			// Ricarica la pagina
			location.reload(true);
		}
	}

	/*-----------------------------*/
	/* CLIENT LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Aggiunge un gestore di eventi per il bottone "LogIn"
	btnLogIn.addEventListener("click", function () {
		logInPrompt();
	});

	// Aggiunge un gestore di eventi per il bottone "CreateRoom"
	btnCreateRoom.addEventListener("click", function () {
		roomPrompt("create");
	});

	// Aggiunge un gestore di eventi per il bottone "JoinRoom"
	btnJoinRoom.addEventListener("click", function () {
		roomPrompt("join");
	});

	// Aggiunge un gestore di eventi per il bottone "LogOut"
	btnLogOut.addEventListener("click", function () {
		logOutConfirm();
	});
};