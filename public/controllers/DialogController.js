var DialogController = function (btnDialog) {
	//
	var btnDialog = document.getElementById("btnDialog");

	// Crea dinamicamente elementi html
	function createHtmlElement(elementType, elementClass, elementId, elementFatherId, innerHTML){
		var element = document.createElement(elementType);
		var elementFather = document.getElementById(elementFatherId);
		if (elementId != "") {
			element.id = elementId;
		}
		if (elementId != "") {
			element.className = elementClass;
		}
		elementFather.appendChild(element);
		if (innerHTML != "") {
			var span = document.createElement("span");
			span.innerHTML = innerHTML;
			element.appendChild(span);
		}
		return element;
	}
	// Rimuove elementi html
	function removeHtmlElement(element){
		element.parentNode.removeChild(element);
	}
	btnDialog.addEventListener("click", function(){
		var dialogLayer = createHtmlElement("div", "", "dialogLayer", "z-index3", "");

		var dialog = createHtmlElement("div", "", "dialog", "z-index3", "");

		var dialogTitle = createHtmlElement("div", "", "dialogTitle", "dialog", "Titolo");

		var dialoginputText1 = createHtmlElement("input", "dialogInputText", "dialoginputText1", "dialog", "");
		dialoginputText1.setAttribute("type", "text");
		dialoginputText1.placeholder = "Type here..";

		var dialoginputText2 = createHtmlElement("input", "dialogInputText", "dialoginputText2", "dialog", "");
		dialoginputText2.setAttribute("type", "text");
		dialoginputText2.placeholder = "Type here..";
		
		var dialogAlert = createHtmlElement("div", "", "dialogAlert", "dialog", "avviso supercalifragilistichespiralidoso");

		var dialogButtons = createHtmlElement("div", "", "dialogButtons", "dialog", "");

		var dialogPositiveButton = createHtmlElement("div", "dialogButtons", "dialogPositiveButton", "dialogButtons", "Ok");
		var dialogNegativeButton = createHtmlElement("div", "dialogButtons", "dialogNegativeButton", "dialogButtons", "Cancel");

		dialogPositiveButton.addEventListener("click", function(){
			removeHtmlElement(dialog);
			removeHtmlElement(dialogLayer);
		});

		dialogNegativeButton.addEventListener("click", function(){
			removeHtmlElement(dialog);
			removeHtmlElement(dialogLayer);
		});
	});
};