var WhiteBoardController = function (whiteBoardModel, commandsController, canvasWrapperId) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabili per referenziare gli elementi del DOM
	var canvasWrapper = document.getElementById(canvasWrapperId);
	canvasWrapper.width = canvasWrapper.offsetWidth;
	canvasWrapper.height = canvasWrapper.offsetHeight;
	
	// Flag di stato
	var isMouseDown = false;
	var mouseDownCursor;

	/*-----------------*/
	/* PRIVATE METHODS */
	/*-----------------*/
	// Metodo privato che restituisce le coordinate del mouse o touch
	function getCursor(event) {

		var cursor = {x: 0, y: 0};
		var textRectangle = canvasWrapper.getBoundingClientRect();
		if (event.type.indexOf("touch") !== -1) {
			cursor.x = event.touches[0].clientX;
			cursor.y = event.touches[0].clientY;
		} else {
			cursor.x = event.clientX;
			cursor.y = event.clientY;
		}
		return {
			x: Math.floor(cursor.x - textRectangle.left),
			y: Math.floor(cursor.y - textRectangle.top)
		};
	}

	/*-----------------------------*/
	/* CLIENT LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Aggiunge un gestore di eventi per mousedown/touchstart
	canvasWrapper.addEventListener("mousedown", onMouseDown, false);
	canvasWrapper.addEventListener("touchstart", onMouseDown, false);
	canvasWrapper.addEventListener("pointerdown", onMouseDown, false);
	function onMouseDown(event) {
		
		if(event.which != 3) {
			var cursor = getCursor(event);

			isMouseDown = true;
			mouseDownCursor = {x: cursor.x, y: cursor.y};

			//whiteBoardModel.addDot(cursor);

			if (commandsController.getCurrentTool() == "rect" || commandsController.getCurrentTool() == "ellipse") {
				whiteBoardModel.addSketchWithoutDot(cursor);
			}
			else {
				whiteBoardModel.addSketchWithDot(cursor);
			};
		}
	}

	// Aggiunge un gestore di eventi per mousemove/touchmove
	canvasWrapper.addEventListener("mousemove", function(event){
		event.preventDefault(); //Previene le gestures
	}, false);

	canvasWrapper.addEventListener("touchmove", function(event){
		event.preventDefault(); //Previene le gestures
	}, false);

	canvasWrapper.addEventListener("pointermove", function(event){
		event.preventDefault(); //Previene le gestures
	}, false);

	var lastCall = 0;
	window.addEventListener("mousemove", onMouseMove, false);
	window.addEventListener("touchmove", onMouseMove, false);
	window.addEventListener("pointermove", onMouseMove, false);
	function onMouseMove(event) {
		// limita l'esecuzione a massimo 120 volte al secondo
		if(Date.now() - lastCall > 1000/120) {
			
			if (isMouseDown == true) {
				var cursor = getCursor(event);

				if (commandsController.getCurrentTool() == "line") {
					whiteBoardModel.addLine(cursor);
				}
				else if (commandsController.getCurrentTool() == "rect") {
					whiteBoardModel.addRect(cursor);
				}
				else if (commandsController.getCurrentTool() == "ellipse") {
					whiteBoardModel.addEllipse(mouseDownCursor, cursor);
				}
				else{
					whiteBoardModel.addStroke(cursor);
				};

				lastCall = Date.now();
			}
		}
	}

	// Aggiunge un gestore di eventi per mouseup/touchend
	window.addEventListener("mouseup", onMouseUp, false);
	window.addEventListener("touchend", onMouseUp, false);
	window.addEventListener("pointerup", onMouseUp, false);
	function onMouseUp(event) {
		isMouseDown = false;
	}

	// Aggiunge un gestore di eventi per resize
	window.addEventListener("resize", onResize, false);
	function onResize() {
		whiteBoardModel.updateCanvasSize();
	}
};