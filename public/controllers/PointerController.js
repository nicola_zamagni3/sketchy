var PointerController = function (pointerModel, canvasWrapperId) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabili per referenziare gli elementi del DOM
	var canvasWrapper = document.getElementById(canvasWrapperId);
	canvasWrapper.width = canvas.offsetWidth;
	canvasWrapper.height = canvas.offsetHeight;

	/*-----------------*/
	/* PRIVATE METHODS */
	/*-----------------*/
	// Metodo privato che restituisce le coordinate del mouse o touch
	function getCursor(event) {

		var cursor = {x: 0, y: 0};
		var textRectangle = canvasWrapper.getBoundingClientRect();
		if (event.type.indexOf("touch") !== -1) {
			cursor.x = event.touches[0].clientX;
			cursor.y = event.touches[0].clientY;
		} else {
			cursor.x = event.clientX;
			cursor.y = event.clientY;
		}
		return {
			x: Math.floor(cursor.x - textRectangle.left),
			y: Math.floor(cursor.y - textRectangle.top)
		};
	}

	/*-----------------------------*/
	/* CLIENT LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Aggiunge un gestore di eventi per mousemove/touchmove
	var lastCall = 0;
	canvasWrapper.addEventListener("mousemove", onMouseMove, false);
	canvasWrapper.addEventListener("touchmove", onMouseMove, false);
	canvasWrapper.addEventListener("touchstart", onMouseMove, false);
	function onMouseMove(event) {
		// limita l'esecuzione a massimo 120 volte al secondo
		if(Date.now() - lastCall > 1000/120) {
			var cursor = getCursor(event);
			
			pointerModel.emitCursorMove(cursor);
			
			lastCall = Date.now();
		}
	}

	// Aggiunge un gestore di eventi per resize
	window.addEventListener("resize", onResize, false);
	function onResize() {
		pointerModel.updateCanvasSize();
	}
};