var ChatController = function (chatModel, parameters) {
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	// Variabili per referenziare gli elementi del DOM
	var form = document.getElementById(parameters.form);
	var inputText = document.getElementById(parameters.inputText);

	/*-----------------------------*/
	/* CLIENT LISTENERS & HANDLERS */
	/*-----------------------------*/
	// Aggiunge un gestore di eventi per submit
	form.addEventListener("submit", function(event) {
		// Comunica al server l'evento "ClientEventNewMessage"
		if (inputText.value != "") {
			chatModel.emitNewMessage(inputText.value);
			
			inputText.value = "";
		}
		inputText.focus();
	}, false);
};