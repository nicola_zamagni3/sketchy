var MenusController = function(parameters) {

	// Variabili per referenziare gli elementi del DOM
	var wrapper = document.getElementById(parameters.wrapper);
	var menusLayer = document.getElementById(parameters.menusLayer);
	var transitionDuration = parseFloat(getComputedStyle(menusLayer)["transitionDuration"]);

	var btnMenu1 = document.getElementById(parameters.btnMenu1);
	var btnMenu2 = document.getElementById(parameters.btnMenu2);

	var menu1 = document.getElementById(parameters.menu1);
	var menu2 = document.getElementById(parameters.menu2);

	function slideIn(menu) {
		menusLayer.className = "display";
		menusLayer.className = "fadeOut";
		menu.className = "slideIn";
	}
	function slideOut(menu) {
		menu.className = "slideOut";
		menusLayer.className = "fadeIn";
		//aspetta per far terminare la transizione
		setTimeout(function(){
			menusLayer.className = "hidden";
		},1000*transitionDuration);
	}
	// Aggiunge un gestore di eventi per "btnMenu1"
	btnMenu1.addEventListener("click", function(){slideIn(menu1);});

	// Aggiunge un gestore di eventi per "btnMenu2"
	btnMenu2.addEventListener("click", function(){
		slideIn(menu2);
		btnMenu2.style.backgroundImage = "url(\"./views/assets/icons/notification.svg\")";
	});

	// Aggiunge un gestore di eventi per "menusLayer"
	menusLayer.addEventListener("click", function(){
		if (menu1.className == "slideIn") {
			slideOut(menu1);
		}
		else if(menu2.className == "slideIn") {
			slideOut(menu2);
		}
	});
};