var CommandsController = function(parameters){
	
	/*-------------------*/
	/* PRIVATE VARIABLES */
	/*-------------------*/
	var whiteBoardModel = parameters.whiteBoardModel;

	// Variabili per referenziare gli elementi del DOM
	var btnNew = document.getElementById(parameters.btnNew);
	var btnOpen = document.getElementById(parameters.btnOpen);
	var btnSave = document.getElementById(parameters.btnSave);
	var btnExport = document.getElementById(parameters.btnExport);

	var btnUndo = document.getElementById(parameters.btnUndo);
	var btnRedo = document.getElementById(parameters.btnRedo);

	var btnPencil = document.getElementById(parameters.btnPencil);
	var btnEraser = document.getElementById(parameters.btnEraser);
	var btnMarker = document.getElementById(parameters.btnMarker);
	var btnLine = document.getElementById(parameters.btnLine);
	var btnRect = document.getElementById(parameters.btnRect);
	var btnEllipse = document.getElementById(parameters.btnEllipse);

	var btnWidth1 = document.getElementById(parameters.btnWidth1);
	var btnWidth2 = document.getElementById(parameters.btnWidth2);
	var btnWidth3 = document.getElementById(parameters.btnWidth3);
	var btnWidth4 = document.getElementById(parameters.btnWidth4);
	var btnWidth5 = document.getElementById(parameters.btnWidth5);

	var btnColor = document.getElementById(parameters.btnColor);

	var btnFullscreen = document.getElementById(parameters.btnFullscreen);

	var currentTool = "pencil";
	var currentToolsSettings = {
		pencil: {
			width: 2,
			color: "#000000",
		},
		eraser: {
			width: 8,
			color: "#ffffff",
		},
		marker: {
			width: 32,
			color: "#ffff00",
		},
		line: {
			width: 2,
			color: "#000000",
		},
		rect: {
			width: 2,
			color: "#000000",
		},
		ellipse: {
			width: 2,
			color: "#000000",
		},
	};

	/*-----------------*/
	/* PRIVATE METHODS */
	/*-----------------*/
	// Metodo privato per settare il selettore del tipo di tool
	function setToolSelector(btn){
		btnPencil.className = "btn";
		btnEraser.className = "btn";
		btnMarker.className = "btn";
		btnLine.className = "btn";
		btnRect.className = "btn";
		btnEllipse.className = "btn";

		btn.className = "currentTool";
	}

	// Metodo privato per settare il selettore dello spessore del tool
	function setWidthSelector(toolWidth){
		btnWidth1.className = "btn";
		btnWidth2.className = "btn";
		btnWidth3.className = "btn";
		btnWidth4.className = "btn";
		btnWidth5.className = "btn";

		switch(toolWidth) {
			case 2:
				btnWidth1.className = "currentWidth";
			break;
			case 4:
				btnWidth2.className = "currentWidth";
			break;
			case 8:
				btnWidth3.className = "currentWidth";
			break;
			case 16:
				btnWidth4.className = "currentWidth";
			break;
			case 32:
				btnWidth5.className = "currentWidth";
			break;
		}
	}

	// Metodo privato per settare il selettore del colore del tool
	function setColorSelector(toolColor){
		document.querySelector("label[for=\"btnColor\"]").style.backgroundColor = toolColor;
	}

	// Metodo privato per settare lo strumento
	function setTool(toolName, toolWidth, toolColor){
		btnColor.disabled = false;

		if (toolName == "pencil") {
			currentTool = "pencil";
			currentToolsSettings.pencil.width = toolWidth;
			currentToolsSettings.pencil.color = toolColor;

			whiteBoardModel.setGlobalCompositeOperation("source-over");
			whiteBoardModel.setLineWidth(toolWidth);
			whiteBoardModel.setStrokeStyle(toolColor);
			
			setToolSelector(btnPencil);
			setWidthSelector(toolWidth);
			setColorSelector(toolColor);
		}
		else if (toolName == "eraser") {
			currentTool = "eraser";
			currentToolsSettings.eraser.width = toolWidth;
			currentToolsSettings.eraser.color = "#ffffff";
			
			whiteBoardModel.setGlobalCompositeOperation("destination-out");
			whiteBoardModel.setLineWidth(toolWidth);
			whiteBoardModel.setStrokeStyle("#ffffff");
			btnColor.disabled = true;
			
			setToolSelector(btnEraser);
			setWidthSelector(toolWidth);
			setColorSelector("#ffffff");
		}
		else if (toolName == "marker") {
			currentTool = "marker";
			currentToolsSettings.marker.width = toolWidth;
			currentToolsSettings.marker.color = toolColor;

			whiteBoardModel.setGlobalCompositeOperation(/*"destination-over"*//*"multiply"*/"darken");
			whiteBoardModel.setLineWidth(toolWidth);
			whiteBoardModel.setStrokeStyle(toolColor);

			setToolSelector(btnMarker);
			setWidthSelector(toolWidth);
			setColorSelector(toolColor);
		}
		else if (toolName == "line") {
			currentTool = "line";
			currentToolsSettings.line.width = toolWidth;
			currentToolsSettings.line.color = toolColor;

			whiteBoardModel.setGlobalCompositeOperation("source-over");
			whiteBoardModel.setLineWidth(toolWidth);
			whiteBoardModel.setStrokeStyle(toolColor);
			
			setToolSelector(btnLine);
			setWidthSelector(toolWidth);
			setColorSelector(toolColor);
		}
		else if (toolName == "rect") {
			currentTool = "rect";
			currentToolsSettings.rect.width = toolWidth;
			currentToolsSettings.rect.color = toolColor;

			whiteBoardModel.setGlobalCompositeOperation("source-over");
			whiteBoardModel.setLineWidth(toolWidth);
			whiteBoardModel.setStrokeStyle(toolColor);
			
			setToolSelector(btnRect);
			setWidthSelector(toolWidth);
			setColorSelector(toolColor);
		}
		else if (toolName == "ellipse") {
			currentTool = "ellipse";
			currentToolsSettings.ellipse.width = toolWidth;
			currentToolsSettings.ellipse.color = toolColor;

			whiteBoardModel.setGlobalCompositeOperation("source-over");
			whiteBoardModel.setLineWidth(toolWidth);
			whiteBoardModel.setStrokeStyle(toolColor);
			
			setToolSelector(btnEllipse);
			setWidthSelector(toolWidth);
			setColorSelector(toolColor);
		}
	}
	// Di default imposta la penna nera
	setTool("pencil", 2, "#000000");

	
	/*----------------*/
	/* PUBLIC METHODS */
	/*----------------*/
	this.getCurrentTool = function(){
		return currentTool;
	}

	/*-----------------------------*/
	/* CLIENT LISTENERS & HANDLERS */
	/*-----------------------------*/
	/* COMMANDS */
	// Aggiunge un gestore di eventi per il bottone "New"
	btnNew.addEventListener("click", function () {
		if (confirm("Are you sure? All unsaved changes will be lost!") == true) {
			whiteBoardModel.new();
		}
	});

	// Aggiunge un gestore di eventi per il bottone "Open"
	btnOpen.addEventListener("change", function (event) {
		var file = event.target.files[0];
		whiteBoardModel.open(file);
		btnOpen.value = null;
	});

	// Aggiunge un gestore di eventi per il bottone "Save"
	btnSave.addEventListener("click", function (event) {
		var filename = prompt("Enter file name","whiteBoardModel");
		if ( filename === null) {
			event.preventDefault();
		}
		else if ( filename != null) {
			if (/[^\\\/:*?"<>|\r\n]+$/im.test(filename)) {
				filename = filename + ".json";
			}
			else {
				filename = "whiteBoardModel.json";
			}
			whiteBoardModel.save(this, filename);
		}
	});

	// Aggiunge un gestore di eventi per il bottone "Export"
	btnExport.addEventListener("click", function (event) {
		var filename = prompt("Enter file name","whiteBoardModel");
		if ( filename === null) {
			event.preventDefault();
		}
		if ( filename != null) {
			if (/[^\\\/:*?"<>|\r\n]+$/im.test(filename)) {
				filename = filename + ".png";
			}
			else {
				filename = "whiteBoardModel.png";
			}
			whiteBoardModel.export(this, filename);
		}
	});
	
	// Aggiunge un gestore di eventi per il bottone "Undo"
	btnUndo.addEventListener("click", function () {
		whiteBoardModel.undo();
	});
	// Aggiunge un gestore di eventi per Ctrl+z
	window.addEventListener("keydown", onCtrlz, false);
	function onCtrlz(event) {
		if (event.keyCode == 90 && event.ctrlKey){
			whiteBoardModel.undo();
		}
	}

	// Aggiunge un gestore di eventi per il bottone "Redo"
	btnRedo.addEventListener("click", function () {
		whiteBoardModel.redo();
	});

	// Aggiunge un gestore di eventi per il drag & drop dell'immagine
	document.getElementById(whiteBoardModel.getCanvasId()).addEventListener("dragover", function (event) {
		event.preventDefault();
	}, false);
	document.getElementById(whiteBoardModel.getCanvasId()).addEventListener("drop", function (event) {
		event.preventDefault();
		var file = event.dataTransfer.files[0];
		whiteBoardModel.open(file);
	}, false);

	// Aggiunge un gestore di eventi per l'incolla di un immagine dagli appunti
	window.addEventListener("paste", onCtrlv, false);
	function onCtrlv(event) {
		var items = (event.clipboardData || event.originalEvent.clipboardData).items;
		for (var i = 0; i < items.length; i++) {
			if (items[i].type.indexOf("image") === 0) {
				var file = items[i].getAsFile();
				whiteBoardModel.open(file);
			}
		}
	}

	/* TOOLS */
	// Aggiunge un gestore di eventi per il bottone "Pen"
	btnPencil.addEventListener("click", function () {
		var toolWidth = currentToolsSettings.pencil.width;
		var toolColor = currentToolsSettings.pencil.color;

		setTool("pencil", toolWidth, toolColor);
	});

	// Aggiunge un gestore di eventi per il bottone "Eraser"
	btnEraser.addEventListener("click", function () {
		var toolWidth = currentToolsSettings.eraser.width;
		var toolColor = "#ffffff";

		setTool("eraser", toolWidth, toolColor);
	});
	
	// Aggiunge un gestore di eventi per il bottone "Marker"
	btnMarker.addEventListener("click", function () {
		var toolWidth = currentToolsSettings.marker.width;
		var toolColor = currentToolsSettings.marker.color;

		setTool("marker", toolWidth, toolColor);
	});

	// Aggiunge un gestore di eventi per il bottone "Line"
	btnLine.addEventListener("click", function () {
		var toolWidth = currentToolsSettings.line.width;
		var toolColor = currentToolsSettings.line.color;

		setTool("line", toolWidth, toolColor);
	});
	// Aggiunge un gestore di eventi per il bottone "Rect"
	btnRect.addEventListener("click", function () {
		var toolWidth = currentToolsSettings.rect.width;
		var toolColor = currentToolsSettings.rect.color;

		setTool("rect", toolWidth, toolColor);
	});
	// Aggiunge un gestore di eventi per il bottone "Ellipse"
	btnEllipse.addEventListener("click", function () {
		var toolWidth = currentToolsSettings.ellipse.width;
		var toolColor = currentToolsSettings.ellipse.color;

		setTool("ellipse", toolWidth, toolColor);
	});

	// Aggiunge un gestore di eventi per il bottone "Width1"
	btnWidth1.addEventListener("click", function () {
		if (currentTool == "pencil") {
			setTool(currentTool, 2, currentToolsSettings.pencil.color);
		}
		else if (currentTool == "eraser") {
			setTool(currentTool, 2, currentToolsSettings.eraser.color);
		}
		else if (currentTool == "marker") {
			setTool(currentTool, 2, currentToolsSettings.marker.color);
		}
		else if (currentTool == "line") {
			setTool(currentTool, 2, currentToolsSettings.line.color);
		}
		else if (currentTool == "rect") {
			setTool(currentTool, 2, currentToolsSettings.rect.color);
		}
		else if (currentTool == "ellipse") {
			setTool(currentTool, 2, currentToolsSettings.ellipse.color);
		}
	});

	// Aggiunge un gestore di eventi per il bottone "Width2"
	btnWidth2.addEventListener("click", function () {
		if (currentTool == "pencil") {
			setTool(currentTool, 4, currentToolsSettings.pencil.color);
		}
		else if (currentTool == "eraser") {
			setTool(currentTool, 4, currentToolsSettings.eraser.color);
		}
		else if (currentTool == "marker") {
			setTool(currentTool, 4, currentToolsSettings.marker.color);
		}
		else if (currentTool == "line") {
			setTool(currentTool, 4, currentToolsSettings.line.color);
		}
		else if (currentTool == "rect") {
			setTool(currentTool, 4, currentToolsSettings.rect.color);
		}
		else if (currentTool == "ellipse") {
			setTool(currentTool, 4, currentToolsSettings.ellipse.color);
		}
	});

	// Aggiunge un gestore di eventi per il bottone "Width3"
	btnWidth3.addEventListener("click", function () {
		if (currentTool == "pencil") {
			setTool(currentTool, 8, currentToolsSettings.pencil.color);
		}
		else if (currentTool == "eraser") {
			setTool(currentTool, 8, currentToolsSettings.eraser.color);
		}
		else if (currentTool == "marker") {
			setTool(currentTool, 8, currentToolsSettings.marker.color);
		}
		else if (currentTool == "line") {
			setTool(currentTool, 8, currentToolsSettings.line.color);
		}
		else if (currentTool == "rect") {
			setTool(currentTool, 8, currentToolsSettings.rect.color);
		}
		else if (currentTool == "ellipse") {
			setTool(currentTool, 8, currentToolsSettings.ellipse.color);
		}
	});

	// Aggiunge un gestore di eventi per il bottone "Width4"
	btnWidth4.addEventListener("click", function () {
		if (currentTool == "pencil") {
			setTool(currentTool, 16, currentToolsSettings.pencil.color);
		}
		else if (currentTool == "eraser") {
			setTool(currentTool, 16, currentToolsSettings.eraser.color);
		}
		else if (currentTool == "marker") {
			setTool(currentTool, 16, currentToolsSettings.marker.color);
		}
		else if (currentTool == "line") {
			setTool(currentTool, 16, currentToolsSettings.line.color);
		}
		else if (currentTool == "rect") {
			setTool(currentTool, 16, currentToolsSettings.rect.color);
		}
		else if (currentTool == "ellipse") {
			setTool(currentTool, 16, currentToolsSettings.ellipse.color);
		}
	});

	// Aggiunge un gestore di eventi per il bottone "Width5"
	btnWidth5.addEventListener("click", function () {
		if (currentTool == "pencil") {
			setTool(currentTool, 32, currentToolsSettings.pencil.color);
		}
		else if (currentTool == "eraser") {
			setTool(currentTool, 32, currentToolsSettings.eraser.color);
		}
		else if (currentTool == "marker") {
			setTool(currentTool, 32, currentToolsSettings.marker.color);
		}
		else if (currentTool == "line") {
			setTool(currentTool, 32, currentToolsSettings.line.color);
		}
		else if (currentTool == "rect") {
			setTool(currentTool, 32, currentToolsSettings.rect.color);
		}
		else if (currentTool == "ellipse") {
			setTool(currentTool, 32, currentToolsSettings.ellipse.color);
		}
	});

	// Aggiunge un gestore di eventi per il bottone "Color"
	btnColor.addEventListener("change", function () {
		if (currentTool == "pencil") {
			setTool(currentTool, currentToolsSettings.pencil.width, btnColor.value);
		}
		else if (currentTool == "eraser") {
			setTool(currentTool, currentToolsSettings.eraser.width, btnColor.value);
		}
		else if (currentTool == "marker") {
			setTool(currentTool, currentToolsSettings.marker.width, btnColor.value);
		}
		else if (currentTool == "line") {
			setTool(currentTool, currentToolsSettings.line.width, btnColor.value);
		}
		else if (currentTool == "rect") {
			setTool(currentTool, currentToolsSettings.rect.width, btnColor.value);
		}
		else if (currentTool == "ellipse") {
			setTool(currentTool, currentToolsSettings.ellipse.width, btnColor.value);
		}
	});

	// Aggiunge un gestore di eventi per il bottone "Fullscreen"
	btnFullscreen.addEventListener("click", function () {
		var element = document.body;
		if (element.requestFullscreen) {
			element.requestFullscreen();
		}
		else if (element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		}
		else if (element.webkitRequestFullScreen) {
			element.webkitRequestFullScreen();
		}
		else if (element.msRequestFullscreen) {
			element.msRequestFullscreen();
		}
	});
};